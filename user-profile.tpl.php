<?php

/**
 * @file
 * Default theme implementation to present all user profile data.
 *
 * This template is used when viewing a registered member's profile page,
 * e.g., example.com/user/123. 123 being the users ID.
 *
 * Use render($user_profile) to print all profile items, or print a subset
 * such as render($user_profile['user_picture']). Always call
 * render($user_profile) at the end in order to print all remaining items. If
 * the item is a category, it will contain all its profile items. By default,
 * $user_profile['summary'] is provided, which contains data on the user's
 * history. Other data can be included by modules. $user_profile['user_picture']
 * is available for showing the account picture.
 *
 * Available variables:
 *   - $user_profile: An array of profile items. Use render() to print them.
 *   - Field variables: for each field instance attached to the user a
 *     corresponding variable is defined; e.g., $account->field_example has a
 *     variable $field_example defined. When needing to access a field's raw
 *     values, developers/themers are strongly encouraged to use these
 *     variables. Otherwise they will have to explicitly specify the desired
 *     field language, e.g. $account->field_example['en'], thus overriding any
 *     language negotiation rule that was previously applied.
 *
 * @see user-profile-category.tpl.php
 *   Where the html is handled for the group.
 * @see user-profile-item.tpl.php
 *   Where the html is handled for each item in the group.
 * @see template_preprocess_user_profile()
 */
?>
<?php 
$viewing_uid = arg(1);
$account = menu_get_object('user');
if (is_numeric($viewing_uid)):
	$user_data = user_load($viewing_uid);
	if (is_array($user_data->roles) && in_array('admin', $user_data->roles)) {
		$is_admin = TRUE;
	}

// edit button for user themselves
	if ($user->uid == $viewing_uid) {
		echo '<div style="float:right"><p><a class="btn grey" href="/user/'.$user->uid.'/edit" >View/Edit Your Profile</a></p>';
		echo '</div>';
	} 
	$show_user_flag = TRUE;
	if ($user->uid == $viewing_uid) {
		$show_user_flag = FALSE;
	}
	if ($is_admin) {
		$show_user_flag = TRUE;
	}
	if ($show_user_flag == TRUE) {
		echo '<div id="flag-container" style="float:right; clear:right">';
		$flag = flag_get_flag('flag_user');
		if (($flag->is_flagged($account->uid)) and ($is_admin == FALSE)) {
			// display functional but redundant flag button
		    echo '<span class="flag-wrapper flag-flag-user flag-flag-user-14">
		      <a href="/flag/confirm/flag/flag_user/'.$viewing_uid .'?destination=user/'.$viewing_uid .'" title="Flag this user for review" class="flag flag-action flag-link-confirm" rel="nofollow">Flag User</a><span class="flag-throbber">&nbsp;</span>
		    </span>';
	  	} else {
	   		print flag_create_link('flag_user', $account->uid);
		}
	  echo '</div>';
	}

	echo '<h1 id="page-title" class="title">'.$user_data->field_user_firstname['und'][0]['safe_value'].' '.$user_data->field_user_lastname['und'][0]['safe_value'];
	if (isset($user_data->field_user_number_of_exchanges['und'][0]['value'])) {
		//display number of exchanges linked to explanation page pop-up
		echo '(';
		print l($user_data->field_user_number_of_exchanges['und'][0]['value'], 'help/exchange-completed-note', array('attributes' => array('class' => array('simple-dialog'), 'name' => 'block-system-main', 'rel' => 'width:950;resizable:false;position:[center,60]', 'title' => 'Completed exchange notes')));
		echo ')';
	}
	if (ge_convert_stars_to_classes(ge_get_user_rating($user_data->uid)) != '') {
		echo '<span class="display-stars '.ge_convert_stars_to_classes(ge_get_user_rating($user_data->uid)).'" style="">&nbsp;</span>';
	}
    echo '</h1>';
	if ($user->uid == 0): // not logged in ?>
    
    <p><a href="/user">Log in</a> to view more user details</p>

<?php else: // logged in 
	?><div style="clear:both">
	<?php if (isset($user_data->picture->filename)): ?>
			<img typeof="foaf:Image" src="/sites/thegiftingearth.net/files/styles/thumbnail/public/pictures/<?php echo $user_data->picture->filename; ?>" alt="<?php echo $user_data->field_user_firstname['und'][0]['safe_value'].' '.$user_data->field_user_lastname['und'][0]['safe_value']; ?>&#039;s picture" title="<?php echo $user_data->field_user_firstname['und'][0]['safe_value'].' '.$user_data->field_user_lastname['und'][0]['safe_value']; ?>&#039;s picture" class="user-profile-image" />
	<?php endif; ?> 
	<p><strong>Member Since:</strong> <?php echo date('F Y', $user_data->created); ?><br />
    <strong>Location:</strong> <?php echo $user_data->location['city'].', '.$user_data->location['province'].' '.$user_data->location['postal_code']; ?><br />
    <?php if (arg(0) == 'user' && is_numeric(arg(1)) ) {
	  $url = privatemsg_get_link( array( user_load(arg(1)) ) );
	  if ($user->uid != arg(1)) {
	  	print l(t('Send me a Message'), $url, array('query' => drupal_get_destination()));
	  }
	} ?></p>
	<?php if (isset($user_data->field_user_description['und'][0]['safe_value'])):
				echo $user_data->field_user_description['und'][0]['safe_value']; 
		 endif; ?>
		
   
    </div>
    <h2><?php print t('Completed Exchanges'); ?></h2>

   <?php $block = module_invoke('views', 'block_view', 'user_profile_summary-block_1');
  print render($block);?>
    
<?php endif; ?>

<?php endif; ?>