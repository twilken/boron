<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/garland.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 */
 
 
 // check if exchange node
 if (isset($node)) {
	  	if ($node->type === 'exchange') {
			$not_exchange = FALSE;
		} else {
			$not_exchange = TRUE;
		}
 } else {
	 $not_exchange = TRUE;
 }
 
 // check if add exchange node
 if ($_SERVER['REQUEST_URI'] == '/node/add/exchange') {
	 $add_exchange = TRUE;
 } else {
	 $add_exchange = FALSE;
 }
 // method to consistently get url arguments();
 $url_components = explode('/', request_uri());
?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=353619911383970";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="page-wrapper"><div id="page"<?php if (!$is_admin) { echo ' class="notadmin"'; } ?>>

  <header id="header" role="banner"><div class="section clearfix">
	<div id="session">
    <?php if (user_is_logged_in()) { 
		global $user;
		$user_data = user_load( $user->uid );
		if (isset($user_data->picture->filename)) {
			$userbgimage = 'class="userlink" style="padding-left:30px; background-image:url(/sites/thegiftingearth.net/files/styles/user_thumbnail/public/pictures/'.$user_data->picture->filename.');"';
		} else {
			$userbgimage = '';
		}
	?>
    <p><a href="/user/<?php echo $user->uid; ?>" <?php echo $userbgimage; ?>><?php echo $user->name; ?></a></br />
    		<?php if (privatemsg_unread_count($user) >0 ) {
				echo '<a href="/messages"><span class="marker">'.privatemsg_unread_count($user).' New Message';
				if (privatemsg_unread_count($user) >1 ) {
					echo 's';
				} 
				echo '</span></a>';
			} else {
				echo '<a href="/messages">Messages</a>';
			} ?><br /><a href="/my-home-page">My Gifts and Needs</a><br /><a href="/user/logout">Log out</a></p>
	  <?php }
	  else {
		echo '<p><a href="/user">User Login</a></p><p><a href="/user/register">New Account</a></p>';
	  }
	
?>
    
    </div>
    
    <?php if ($logo): ?>
      <a href="/" title="<?php print t('Home'); ?>" rel="home" id="logo">
        <img src="/<?php print drupal_get_path('theme','boron'); ?>/images/logo_gifting_earth_sm.gif" alt="<?php print t('Home'); ?>" />
      </a>
    <?php endif; ?>

	<div id="navbar"><div id="navbar-inner" class="clear-block region region-navbar">

          <ul>
          	
            <?php if (user_is_logged_in()) { ?>
            	<li><a class="nav-item" href="/" id="home">HOME</a></li>
            <?php } else { ?>
            	<li><a class="nav-item" href="/" id="home">HOME</a></li>
            <?php } ?>
            <li><a class="nav-item" href="/browse" id="browse">BROWSE</a></li>
            <?php if (user_is_logged_in()) { ?>
            	<li><a class="nav-item" href="/node/add/exchange" id="post">POST</a></li>
            <?php } ?>
			<li><a class="nav-item" href="/forum" id="forums">FORUMS</a></li> 
            <li><a class="nav-item" href="/learn-more" id="learn-more">LEARN MORE</a></li>
          </ul>

        </div></div> <!-- /#navbar-inner, /#navbar -->

    
    <?php if ($is_front): ?>
    	<div id="home-block">
	  		<?php if (!user_is_logged_in()) { 
	  			print drupal_render(drupal_get_form('user_login')); 
	  		} ?>
		    <div id="left-home-block">
		    	<?php if ($page['home_header']): ?>
		    		<?php print render($page['home_header']); ?>
		    	<?php endif; ?>
		    </div><!-- left-home-block -->
            <div style="clear:both"></div>
         </div>
      <?php else: 
	   
	  print render($page['header']); 
	  
	  endif; ?>

    <?php if ($main_menu): ?>
      <p id="skip-link"><em><a href="#navigation">Skip to Navigation</a></em> &darr;</p>
    <?php endif; ?>

  </div></header> <!-- /.section, /#header -->


  <div id="main-wrapper"><div id="main" class="clearfix<?php if ($main_menu) { print ' with-navigation'; } ?>">
  	<div id="sharecontainer">
  		<!-- Lockerz Share BEGIN -->
		<div class="a2a_kit a2a_default_style">
		<a class="a2a_dd" href="http://www.addtoany.com/share_save">Share</a>
		<span class="a2a_divider"></span>
		<a class="a2a_button_facebook"></a>
		<a class="a2a_button_twitter"></a>
		<a class="a2a_button_email"></a>
		<a class="a2a_button_linkedin"></a>
		</div>
		<script type="text/javascript" src="https://static.addtoany.com/menu/page.js"></script>
		<!-- Lockerz Share END -->
	</div>
    <div id="content" class="column" role="main"><div class="section">
      <?php if ($page['highlighted']): ?>
        <div id="highlighted"><?php print render($page['highlighted']); ?></div>
      <?php endif; ?>
      <?php if ($breadcrumb): ?>
        <div id="breadcrumb"><?php print $breadcrumb; ?></div>
      <?php endif; ?>
      <?php print $messages; ?>
      <?php print render($page['help']); ?>
      <?php print render($title_prefix); ?>
      <?php // decide whether or not to show the page title
	  	  	$showtitle = TRUE;
			if (arg(0) == 'user') { // user pages use special template
				if (arg(1) != 'register') {
					$showtitle = FALSE;
				}
			}
			if ($not_exchange == FALSE) { // hide on exchange pages
				$showtitle = FALSE;
			}
			if (arg(1) == 614) { // learn-more page
				$showtitle = FALSE;
			}
			if ($_SERVER['REQUEST_URI'] == '/') { // home page
				$showtitle = FALSE;
			}
			if (arg(0) == 'browse') { // browse pages
				$showtitle = FALSE;
			}
			
			if (($title) and ($showtitle == TRUE)) { ?>
					<h1 class="title" id="page-title"><?php print $title; ?></h1>
		  	<?php }	?>
      <?php print render($title_suffix); ?>
      <?php if ($not_exchange) { 
	  if ((arg(0) != 'user') or (arg(2) == 'edit')) { // hide tabs on certains pages ?>
        <div class="tabs"><?php print render($tabs); ?></div>
      <?php } ?>
      <?php } ?>
      
      
      <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
      <?php print $feed_icons; ?>
    </div></div> <!-- /.section, /#content -->

    <?php if ($page['sidebar_first']): ?>
      <aside id="sidebar-first" class="column sidebar" role="complementary"><div class="section">
        <?php print render($page['sidebar_first']); ?>
      </div></aside> <!-- /.section, /#sidebar-first -->
    <?php endif; ?>

    <?php if ($page['sidebar_second']): ?>
      <aside id="sidebar-second" class="column sidebar" role="complementary"><div class="section">
        <?php print render($page['sidebar_second']); ?>
      </div></aside> <!-- /.section, /#sidebar-second -->
    <?php endif; ?>

  </div></div> <!-- /#main, /#main-wrapper -->
  <footer id="footer" role="contentinfo" style="margin-top:30px"><div class="section">
  	<?php if ($user->uid != 0): ?>
    	<div id="feedback-link">
    		<?php print l(t('Report Problem'), 'feedback', array('query' => array("url" => urlencode($_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'])))); ?>
    	</div>
    <?php endif; ?>

  	<?php if ($page['footer']): ?>
  		<?php print render($page['footer']); ?>
  	<?php endif; ?>
  </div></footer> <!-- /.section, /#footer -->

</div></div> <!-- /#page, /#page-wrapper -->
<?php // prefill text fields on new account page
if (isset($_POST['home-page-signup'])): ?>
<script>
<?php if (isset($_POST['Username'])): ?>
	document.getElementById('edit-name').value = '<?php echo $_POST['Username']; ?>';
<?php endif; ?>
<?php if (isset($_POST['First'])): ?>
	document.getElementById('edit-field-user-firstname-und-0-value').value = '<?php echo $_POST['First']; ?>';
<?php endif; ?>
<?php if (isset($_POST['Last'])): ?>
	document.getElementById('edit-field-user-lastname-und-0-value').value = '<?php echo $_POST['Last']; ?>';
<?php endif; ?>
</script>
<?php endif; ?>

<?php // prefill location from user profile on new exchange nodes
if (($add_exchange == TRUE) and ($logged_in)): 
	//print_r($user_data->location);
	if (!empty($user_data->location)) :?>
		<script>
		// prefill locations fields from user default
			document.getElementById('edit-field-exchange-date-und-0-all-day').checked = true;
			var postingStreet = document.getElementById('edit-field-exchange-location-und-0-street').value;
			var postingStreetAdditional = document.getElementById('edit-field-exchange-location-und-0-additional').value;
			var postingCity = document.getElementById('edit-field-exchange-location-und-0-city').value;
			var postingProvince = document.getElementById('edit-field-exchange-location-und-0-province').value;
			var postingPostalCode = document.getElementById('edit-field-exchange-location-und-0-postal-code').value;
			var postingCountry = document.getElementById('edit-field-exchange-location-und-0-country').value;
			var postingLat = document.getElementById('gmap-auto1map-locpick_latitude0').value;
			var postingLng = document.getElementById('gmap-auto1map-locpick_longitude0').value;
			if (postingStreet == '') {
				document.getElementById('edit-field-exchange-location-und-0-street').value = '<?php echo $user_data->location['street']; ?>';
			}
			if (postingStreetAdditional == '') {
				document.getElementById('edit-field-exchange-location-und-0-additional').value = '<?php echo $user_data->location['additional']; ?>';
			}
			if (postingCity == '') {
				document.getElementById('edit-field-exchange-location-und-0-city').value = '<?php echo $user_data->location['city']; ?>';
			}
			if (postingProvince == '') {
				document.getElementById('edit-field-exchange-location-und-0-province').value = '<?php echo $user_data->location['province']; ?>';
			}
			if (postingPostalCode == '') {
				document.getElementById('edit-field-exchange-location-und-0-postal-code').value = '<?php echo $user_data->location['postal_code']; ?>';
			}
			if (postingCountry == '') {
				document.getElementById('edit-field-exchange-location-und-0-country').value = '<?php echo $user_data->location['country']; ?>';
			}
			if (postingLat == '') {
				document.getElementById('gmap-auto1map-locpick_latitude0').value = '<?php echo $user_data->location['latitude']; ?>';
			}
			if (postingLng == '') {
				document.getElementById('gmap-auto1map-locpick_longitude0').value = '<?php echo $user_data->location['longitude']; ?>';
			}
		</script>
	<?php endif; ?>
<?php endif; ?>


<script>
jQuery('form.exchange_cancel_form').submit(function(event){
     event.preventDefault();
	if (confirm('Are you sure?')) {
        $(this).submit();
    }
});

jQuery('#completetrue').click(function() {
	document.getElementById('completecontainer').style.display = 'block';
	document.getElementById('completequest').style.display = 'none';
	document.getElementById('delivered1').checked = true;
	document.getElementById('delivered2').checked = false;
});
jQuery('#completefalse').click(function() {
	document.getElementById('completecontainer').style.display = 'block';
	document.getElementById('completequest').style.display = 'none';
	document.getElementById('delivered1').checked = false;
	document.getElementById('delivered2').checked = true;
});

jQuery(window).load(function() {
	// load default values
<?php if ($is_front): ?>
		/*document.getElementById('Email').value = 'Email';
		//document.getElementById('Username').value = 'Username';   save for later
		document.getElementById('First').value = 'First';
		document.getElementById('Last').value = 'Last';
	*/
<?php else: ?>
<?php
//hide from Messages filter page 
if (!isset($url_components[1])) {
		$url_components[1] = '';
	} 
if ($url_components[1] != 'messages') :
		
	if ((isset($_GET['search']))) {
		$search_submit = $_GET['search'];
	} else {
		$search_submit = '';
	}
	if ((isset($_GET['uid']))) {
		$uid_submit = $_GET['search'];
	} else {
		$uid_submit = '';
	}
	 if ($search_submit == '') { 
	  ?>
	 var editSearchElement = document.getElementById('edit-search');
		if (editSearchElement != null)
		{
		  document.getElementById('edit-search').value = 'Search';
		}
		
	<?php } 
	if ($uid_submit == '') { 
	
	?>
	 var editUidElement = document.getElementById('edit-uid');
		if (editUidElement != null)
		{
		  document.getElementById('edit-uid').value = 'Username';
		}
	var editUid2Element = document.getElementById('edit-uid--2');
		if (editUid2Element != null)
		{
		  document.getElementById('edit-uid--2').value = 'Username';
		}
		
	<?php  } ?>
	
	<?php  // newsletter signup page
	if ((isset($_POST['Email']))) {
		echo "document.getElementById('edit-submitted-email').value = '".$_POST['Email']."';";
		//$news_Email = $_POST['Email'];
	} else {
		//$news_Email = '';
	}
	if ((isset($_POST['First']))) {
		echo "document.getElementById('edit-field-user-firstname-und-0-value').value = '".$_POST['First']."';";
	} else {
		//$news_First = '';
	}
	if ((isset($_POST['Last']))) {
		echo "document.getElementById('edit-field-user-lastname-und-0-value').value = '".$_POST['Last']."';";
	} else {
		//$news_Last = '';
	}
endif; ?>
	
<?php endif; ?>

jQuery('.form-item-exchange-cancel-form-reason').hide();
	
});

// login form functions
jQuery('#account_checkbox').change(function(){
	var isChecked = jQuery('#account_checkbox').attr('checked')?true:false;
	if(isChecked) {
		// redirect to login
		window.location = "/user/register";
		/*jQuery("#edit-submit").val('CREATE ACCOUNT');
		jQuery("#user-login").attr("action", "/user/register");
		jQuery("#edit-pass").attr("disabled", "disabled");
		jQuery("#edit-name").attr("disabled", "disabled");
		jQuery('label[for=#edit-name"]').css({color:'#8CDCF3'});*/
	} else {
		// login
		jQuery("#edit-submit").val('LOG IN');
		jQuery("#user-login").attr("action", "/user");
		jQuery("#edit-pass").removeAttr("disabled");
		jQuery("#edit-name").removeAttr("disabled");
	}
});

<?php if ($is_front): ?>
// login form custom error message locations
if (jQuery('#edit-pass').is('.error')) {
	jQuery("#edit-pass-error-msg").show();
}
if (jQuery('#edit-name').is('.error')) {
	jQuery("#edit-name-error-msg").show();
}

<?php else: ?>

jQuery("#edit-search")
  .focus(function() { 
        if (this.value === 'Search') {
            this.value = '';
        }
  })
  .blur(function() {
        if (this.value === '') {
            this.value = 'Search';
        }
});
jQuery("#edit-uid")
  .focus(function() { 
        if (this.value === 'Username') {
            this.value = '';
        }
  })
  .blur(function() {
        if (this.value === '') {
            this.value = 'Username';
        }
});

jQuery("#edit-uid--2")
  .focus(function() { 
        if (this.value === 'Username') {
            this.value = '';
        }
  })
  .blur(function() {
        if (this.value === '') {
            this.value = 'Username';
        }
});
<?php endif; ?>

// clear default values on submit

<?php if ($is_front): ?>

<?php else: ?>
jQuery('#views-exposed-form-search-page').submit(function() {
	var searchBox1Value = document.getElementById('edit-search').value;
	if (searchBox1Value === 'Search') {
        document.getElementById('edit-search').value = '';
    }
	var searchBoxValue = document.getElementById('edit-uid');
	if (searchBoxValue != null)
		{
		  if (searchBoxValue.value === 'Username') {
			document.getElementById('edit-uid').value = '';
		}
	}
	
	var searchBox2Value = document.getElementById('edit-uid--2');
	if (searchBox2Value != null)
		{
		  if (searchBox2Value.value === 'Username') {
			document.getElementById('edit-uid--2').value = '';
		}
	}
	
});

// prefill some values on the report problem page
var editSearchElement = document.getElementById('edit-submitted-browser-and-system');
	if (editSearchElement != null)
	{
	  document.getElementById('edit-submitted-browser-and-system').value = '<?php echo $_SERVER['HTTP_USER_AGENT']; ?>';
	}
<?php endif; ?>

function confirmCancel()
{
	var agree=confirm("Are you sure you want to cancel? This action cannot be undone.");
if (agree)
	return true ;
else
	return false ;
}


/* post page */
jQuery("input:radio[field_exchange_longevity[und]]").change(function() {
	var svalue = jQuery('input:radio[field_exchange_longevity[und]]:checked').val();
	  if (svalue == 'as_of_date') {
		  // check show end date
		  jQuery("#edit-field-exchange-date-und-0-show-todate").attr('checked','checked');
		  // display end date
		  jQuery(".form-item-field-exchange-date-und-0-value2").css("display","inline-block");
      } else {
		  // check show end date
		  jQuery("#edit-field-exchange-date-und-0-show-todate").removeAttr("checked");
		  // hide end date
		  jQuery(".form-item-field-exchange-date-und-0-value2").css("display","none");
		  // remove date value
		  jQuery("#edit-field-exchange-date-und-0-value2-datepicker-popup-0").val('');
	  }
});

/* exchange page */
function confirmActivate() {
	var agree=confirm("Please confirm that you are ready to activate the gifting event and want to send your contact instructions?");
	if (agree)
		return true ;
	else
		return false ;
}
jQuery("#exchange-cancel-form").hover(function() {
	jQuery('.form-item-exchange-cancel-form-reason').show('slow');
});

jQuery("#exchange-cancel-form").mouseleave(function() {
	var cancelDialogText = jQuery('.form-item-exchange-cancel-form-reason').val();
	if (!jQuery("*:focus").is("textarea, input")) {
		if (cancelDialogText == '') {
			jQuery('.form-item-exchange-cancel-form-reason').hide();
		} 
	}
});
// rel=external links in a new window
jQuery(document).ready(function() {
	<?php if (isset($_GET['uid'])) {
		if ($_GET['uid'] == ''): ?>
			document.getElementById('edit-uid').value = 'Username';
		<?php endif; 
	} ?>
   	
  jQuery('a[rel*=external]').click(function(){
    window.open(jQuery(this).attr('href'));
    return false; 
  });
});

</script> 