<?php

/**
 * @file
 * Contains theme override functions and preprocess functions for the Boron theme.
 */

/**
 * Changes the default meta content-type tag to the shorter HTML5 version
 */
function boron_html_head_alter(&$head_elements) {
  $head_elements['system_meta_content_type']['#attributes'] = array(
    'charset' => 'utf-8'
  );
}

/**
 * Changes the search form to use the HTML5 "search" input attribute
 */
function boron_preprocess_search_block_form(&$vars) {
  $vars['search_form'] = str_replace('type="text"', 'type="search"', $vars['search_form']);
}

/**
 * Uses RDFa attributes if the RDF module is enabled
 * Lifted from Adaptivetheme for D7, full credit to Jeff Burnz
 * ref: http://drupal.org/node/887600
 */
function boron_preprocess_html(&$vars) {
  if (module_exists('rdf')) {
    $vars['doctype'] = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML+RDFa 1.1//EN">' . "\n";
    $vars['rdf']->version = 'version="HTML+RDFa 1.1"';
    $vars['rdf']->namespaces = $vars['rdf_namespaces'];
    $vars['rdf']->profile = ' profile="' . $vars['grddl_profile'] . '"';
  } else {
    $vars['doctype'] = '<!DOCTYPE html>' . "\n";
    $vars['rdf']->version = '';
    $vars['rdf']->namespaces = '';
    $vars['rdf']->profile = '';
  }
  // makes nid available in html.tpl.php
  $node = menu_get_object();
  if ($node && isset($node->nid)) {
    $node = node_load($node->nid);
    node_build_content($node);
    $vars['nodedata'] = $node->nid;
  } 
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return
 *   A string containing the breadcrumb output.
 */
function boron_breadcrumb($vars) {
  $breadcrumb = $vars['breadcrumb'];
  // Determine if we are to display the breadcrumb.
  $show_breadcrumb = theme_get_setting('breadcrumb_display');
  if ($show_breadcrumb == 'yes') {

    // Optionally get rid of the homepage link.
    $show_breadcrumb_home = theme_get_setting('breadcrumb_home');
    if (!$show_breadcrumb_home) {
      array_shift($breadcrumb);
    }

    // Return the breadcrumb with separators.
    if (!empty($breadcrumb)) {
      $separator = filter_xss(theme_get_setting('breadcrumb_separator'));
      $trailing_separator = $title = '';

      // Add the title and trailing separator
      if (theme_get_setting('breadcrumb_title')) {
        if ($title = drupal_get_title()) {
          $trailing_separator = $separator;
        }
      }
      // Just add the trailing separator
      elseif (theme_get_setting('breadcrumb_trailing')) {
        $trailing_separator = $separator;
      }

      // Assemble the breadcrumb
      return implode($separator, $breadcrumb) . $trailing_separator . $title;
    }
  }
  // Otherwise, return an empty string.
  return '';
}

/**
 * Implementation of theme_tablesort_indicator()
 */
 
function boron_tablesort_indicator($variables) {
  if ($variables['style'] == "asc") {
    return theme('image', array('path' => drupal_get_path('theme','boron') . '/images/arrow_asc.png', 'width' => 15, 'height' => 15, 'alt' => t('sort ascending'), 'title' => t('sort ascending')));
  }
  else {
    return theme('image', array('path' => drupal_get_path('theme','boron') . 'images/arrow_asc.png', 'width' => 15, 'height' => 15, 'alt' => t('sort descending'), 'title' => t('sort descending')));
  }
}

/*
 * Implementation of theme_preprocess_views_exposed_form()
 */
function boron_preprocess_views_exposed_form(&$vars, $hook) {
	  // only alter the required form based on id
	if ($vars['form']['#id'] == 'views-exposed-form-search-page') {
	    // Change the text on the submit button
	    $vars['form']['submit']['#value'] = t('Search');
	    // Rebuild the rendered version (submit button, rest remains unchanged)
	    unset($vars['form']['submit']['#printed']);
	    $vars['button'] = drupal_render($vars['form']['submit']);
	}
}

/* 
 * Implementation of theme_theme()
 */
function boron_theme() {
  $items = array();
  // create custom user-login.tpl.php
  $items['user_login'] = array(
  'render element' => 'form',
  'path' => drupal_get_path('theme', 'boron'),
  'template' => 'user-login',
  'preprocess functions' => array(
  'boron_preprocess_user_login'
  ),
 );
return $items;
}

/*
 * Implementation of theme_form_required_marker()
 * Customize required marker on the First Name field only
 */
function boron_form_required_marker($variables) {
  $text = '*';
  $t = get_t();
  $attributes = array(
    'class' => 'form-required',
    'title' => $t('This field is required.'),
  );
  if ($variables['element']['#title'] == 'First Name') {
    $text = '*'.t('Required Field');
  }
  return '<span' . drupal_attributes($attributes) . '>'.$text.'</span>';
}

/*
 * Implementation of theme_preprocess_page()
 */
function boron_preprocess_page(&$vars, $hook) {
  // only load on registration form
  if ($vars['page']['content']['system_main']['#form_id'] = 'user_register_form') {
    drupal_add_js('profiles/tge/themes/boron/scripts/jquery.maskedinput-1.3.min.js'); // add js for phone formatting
    drupal_add_js('profiles/tge/themes/boron/scripts/country_codes.js'); // add js for phone formatting
  }
    
}

/**
 * Override Logintoboggan form text
 */
function boron_lt_username_description($variables) {
  return '';
}