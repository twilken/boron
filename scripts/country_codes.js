// For New User Registration form
// provides Phone number handling, including adding mask for US numbers, and auto-populating phone with country code on non-US numbers
// used in conjunction with Drupal Phone module and maskedinput-1.3.min.js

jQuery(document).ready(function () {
    // update phone field country code based on Country Select
    var mask = "(999) 999-9999";
    if (jQuery("#edit-field-user-phone-2-und-0-value").val() === '') {
      jQuery("#edit-field-user-phone-2-und-0-value").mask(mask);
    }
    jQuery("select#edit-locations-0-country").change(function(){
      var countryVal = jQuery("select#edit-locations-0-country").val();
        if(countryVal == "us") {
          jQuery("#edit-field-user-phone-2-und-0-value").mask(mask);
        } else {
          jQuery("#edit-field-user-phone-2-und-0-value").unmask(mask);
          // add country code to phone field
          var cCode = getCountryCode(countryVal);
          if (jQuery("#edit-field-user-phone-2-und-0-value").val() != cCode) {
            jQuery("#edit-field-user-phone-2-und-0-value").val(cCode);
          }
        }
    });  
 });

function getCountryCode(countryAbbrievation) {
	var ccode = '';
	switch(countryAbbrievation)
	{
	case 'ca':
		ccode = '+1';
	break;
	case 'us':
		ccode = '+1';
	break;
	case 'ru':
		ccode = '+7';
	break;
	case 'eg':
		ccode = '+20';
	break;
	case 'za':
		ccode = '+27';
	break;
	case 'gr':
		ccode = '+30';
	break;
	case 'nl':
		ccode = '+31';
	break;
	case 'be':
		ccode = '+32';
	break;
	case 'fr':
		ccode = '+33';
	break;
	case 'es':
		ccode = '+34';
	break;
	case 'hu':
		ccode = '+36';
	break;
	case 'it':
		ccode = '+39';
	break;
	case 'va':
		ccode = '+39';
	break;
	case 'ro':
		ccode = '+40';
	break;
	case 'ch':
		ccode = '+41';
	break;
	case 'at':
		ccode = '+43';
	break;
	case 'gb':
		ccode = '+44';
	break;
	case 'dk':
		ccode = '+45';
	break;
	case 'se':
		ccode = '+46';
	break;
	case 'no':
		ccode = '+47';
	break;
	case 'pl':
		ccode = '+48';
	break;
	case 'de':
		ccode = '+49';
	break;
	case 'pe':
		ccode = '+51';
	break;
	case 'mx':
		ccode = '+52';
	break;
	case 'cu':
		ccode = '+53';
	break;
	case 'ar':
		ccode = '+54';
	break;
	case 'br':
		ccode = '+55';
	break;
	case 'cl':
		ccode = '+56';
	break;
	case 'co':
		ccode = '+57';
	break;
	case 've':   
		ccode = '+58';
	break;
	case 'my':
		ccode = '+60';
	break;
	case 'au':
		ccode = '+61';
	break;
	case 'cc':
		ccode = '+61';
	break;
	case 'cx':
		ccode = '+61';
	break;
	case 'id':
		ccode = '+62';
	break;
	case 'ph':
		ccode = '+63';
	break;
	case 'nz':
		ccode = '+64';
	break;
	case 'sg':
		ccode = '+65';
	break;
	case 'th':
		ccode = '+66';
	break;
	case 'jp':
		ccode = '+81';
	break;
	case 'kr':
		ccode = '+82';
	break;
	case 'vn':
		ccode = '+84';
	break;
	case 'cn':
		ccode = '+86';
	break;
	case 'tr':
		ccode = '+90';
	break;
	case 'in':
		ccode = '+91';
	break;
	case 'pk':
		ccode = '+92';
	break;
	case 'af':
		ccode = '+93';
	break;
	case 'lk':
		ccode = '+94';
	break;
	case 'mm':
		ccode = '+95';
	break;
	case 'ir': 
		ccode = '+98';
	break;
	case 'ma':
		ccode = '+212';
	break;
	case 'dz':
		ccode = '+213';
	break;
	case 'tn':
		ccode = '+216';
	break;
	case 'ly':
		ccode = '+218';
	break;
	case 'gm':
		ccode = '+220';
	break;
	case 'sn':
		ccode = '+221';
	break;
	case 'mr':
		ccode = '+222';
	break;
	case 'ml':
		ccode = '+223';
	break;
	case 'gn':
		ccode = '+224';
	break;
	case 'ci':
		ccode = '+225';
	break;
	case 'bf':
		ccode = '+226';
	break;
	case 'ne':
		ccode = '+227';
	break;
	case 'tg':
		ccode = '+228';
	break;
	case 'bj':
		ccode = '+229';
	break;
	case 'mu':
		ccode = '+230';
	break;
	case 'lr':
		ccode = '+231';
	break;
	case 'sl':
		ccode = '+232';
	break;
	case 'gh':
		ccode = '+233';
	break;
	case 'ng':
		ccode = '+234';
	break;
	case 'td':
		ccode = '+235';
	break;
	case 'cf':
		ccode = '+236';
	break;
	case 'cm':
		ccode = '+237';
	break;
	case 'cv':
		ccode = '+238';
	break;
	case 'st':
		ccode = '+239';
	break;
	case 'gq':
		ccode = '+240';
	break;
	case 'ga':
		ccode = '+241';
	break;
	case 'cg':
		ccode = '+242';
	break;
	case 'cd':
		ccode = '+243';
	break;
	case 'ao':
		ccode = '+244';
	break;
	case 'gw':
		ccode = '+245';
	break;
	case 'io':
		ccode = '+246';
	break;
	case 'sc':
		ccode = '+248';
	break;
	case 'sd':
		ccode = '+249';
	break;
	case 'rw':
		ccode = '+250';
	break;
	case 'et':
		ccode = '+251';
	break;
	case 'so':
		ccode = '+252';
	break;
	case 'dj':
		ccode = '+253';
	break;
	case 'ke':
		ccode = '+254';
	break;
	case 'tz':
		ccode = '+255';
	break;
	case 'ug':
		ccode = '+256';
	break;
	case 'bi':
		ccode = '+257';
	break;
	case 'mz':
		ccode = '+258';
	break;
	case 'zm':
		ccode = '+260';
	break;
	case 'mg':
		ccode = '+261';
	break;
	case 'zw':
		ccode = '+263';
	break;
	case 'na':
		ccode = '+264';
	break;
	case 'mw':
		ccode = '+265';
	break;
	case 'ls':
		ccode = '+266';
	break;
	case 'bw':
		ccode = '+267';
	break;
	case 'sz':
		ccode = '+268';
	break;
	case 'km':
		ccode = '+269';
	break;
	case 'yt':
		ccode = '+269';
	break;
	case 'sh':
		ccode = '+290';
	break;
	case 'er':
		ccode = '+291';
	break;
	case 'aw':
		ccode = '+297';
	break;
	case 'fo':
		ccode = '+298';
	break;
	case 'gl':
		ccode = '+299';
	break;
	case 'gi':
		ccode = '+350';
	break;
	case 'pt':
		ccode = '+351';
	break;
	case 'lu':
		ccode = '+352';
	break;
	case 'ie':
		ccode = '+353';
	break;
	case 'is':
		ccode = '+354';
	break;
	case 'al':
		ccode = '+355';
	break;
	case 'mt':
		ccode = '+356';
	break;
	case 'cy':
		ccode = '+357';
	break;
	case 'fi':
		ccode = '+358';
	break;
	case 'bg':
		ccode = '+359';
	break;
	case 'lt':
		ccode = '+370';
	break;
	case 'lv':
		ccode = '+371';
	break;
	case 'ee':
		ccode = '+372';
	break;
	case 'md':
		ccode = '+373';
	break;
	case 'am':
		ccode = '+374';
	break;
	case 'by':
		ccode = '+375';
	break;
	case 'ad':
		ccode = '+376';
	break;
	case 'mc':
		ccode = '+377';
	break;
	case 'sm':
		ccode = '+378';
	break;
	case 'ua':
		ccode = '+380';
	break;
	case 'rs':
		ccode = '+381';
	break;
	case 'me':
		ccode = '+382';
	break;
	case 'hr':
		ccode = '+385';
	break;
	case 'si':
		ccode = '+386';
	break;
	case 'ba':
		ccode = '+387';
	break;
	case 'mk':  
		ccode = '+389';
	break;
	case 'cz':
		ccode = '+420';
	break;
	case 'sk':
		ccode = '+421';
	break;
	case 'li':
		ccode = '+423';
	break;
	case 'fk':
		ccode = '+500';
	break;
	case 'bz':
		ccode = '+501';
	break;
	case 'gt':
		ccode = '+502';
	break;
	case 'sv':
		ccode = '+503';
	break;
	case 'hn':
		ccode = '+504';
	break;
	case 'ni':
		ccode = '+505';
	break;
	case 'cr':
		ccode = '+506';
	break;
	case 'pa':
		ccode = '+507';
	break;
	case 'pm':
		ccode = '+508';
	break;
	case 'ht':
		ccode = '+509';
	break;
	case 'gp':
		ccode = '+590';
	break;
	case 'bo':
		ccode = '+591';
	break;
	case 'gy':
		ccode = '+592';
	break;
	case 'ec':
		ccode = '+593';
	break;
	case 'gf':
		ccode = '+594';
	break;
	case 'py':
		ccode = '+595';
	break;
	case 'mq':
		ccode = '+596';
	break;
	case 'sr':
		ccode = '+597';
	break;
	case 'uy':
		ccode = '+598';
	break;
	case 'an':
		ccode = '+599';
	break;
	case 'tp':
		ccode = '+670';
	break;
	case 'nf':
		ccode = '+672';
	break;
	case 'bn':
		ccode = '+673';
	break;
	case 'nr':
		ccode = '+674';
	break;
	case 'pg':
		ccode = '+675';
	break;
	case 'to':
		ccode = '+676';
	break;
	case 'sb':
		ccode = '+677';
	break;
	case 'vu':
		ccode = '+678';
	break;
	case 'fj':
		ccode = '+679';
	break;
	case 'pw':
		ccode = '+680';
	break;
	case 'wf':
		ccode = '+681';
	break;
	case 'ck':
		ccode = '+682';
	break;
	case 'nu':
		ccode = '+683';
	break;
	case 'ki':
		ccode = '+686';
	break;
	case 'nc':
		ccode = '+687';
	break;
	case 'tv':
		ccode = '+688';
	break;
	case 'pf':
		ccode = '+689';
	break;
	case 'tk':
		ccode = '+690';
	break;
	case 'fm':
		ccode = '+691';
	break;
	case 'mh':
		ccode = '+692';
	break;
	case 'kp':
		ccode = '+850';
	break;
	case 'hk':
		ccode = '+852';
	break;
	case 'mo':
		ccode = '+853';
	break;
	case 'kh':
		ccode = '+855';
	break;
	case 'la':
		ccode = '+856';
	break;
	case 'bd':
		ccode = '+880';
	break;
	case 'tw':
		ccode = '+886';
	break;
	case 'mv':
		ccode = '+960';
	break;
	case 'lb':
		ccode = '+961';
	break;
	case 'jo':
		ccode = '+962';
	break;
	case 'sy':
		ccode = '+963';
	break;
	case 'iq':
		ccode = '+964';
	break;
	case 'kw':
		ccode = '+965';
	break;
	case 'sa':
		ccode = '+966';
	break;
	case 'ye':
		ccode = '+967';
	break;
	case 'om':
		ccode = '+968';
	break;
	case 'ps':
		ccode = '+970';
	break;
	case 'ae':
		ccode = '+971';
	break;
	case 'il':
		ccode = '+972';
	break;
	case 'bh':
		ccode = '+973';
	break;
	case 'qa':
		ccode = '+974';
	break;
	case 'bt':
		ccode = '+975';
	break;
	case 'mn':
		ccode = '+976';
	break;
	case 'np':
		ccode = '+977';
	break;
	case 'tj':
		ccode = '+992';
	break;
	case 'tm':
		ccode = '+993';
	break;
	case 'az':
		ccode = '+994';
	break;
	case 'ge':
		ccode = '+995';
	break;
	case 'kg':
		ccode = '+996';
	break;
	case 'uz':
		ccode = '+998';
	break;
	default:
		ccode = '';
	}
  return ccode;
}
