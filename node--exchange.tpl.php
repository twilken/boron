<?php

/**
 * @file
 * Custom implementation to display an Exchange node.
 * 
 */
 
// get gofting earth config setting to show or hide rersponse buttons
$hide_ge_response_buttons = ge_config_settings();

// test if this is a listing or response node
$field_giftingearth_node_subtype = $node->field_giftingearth_node_subtype['und'][0]['value'];
if ($field_giftingearth_node_subtype == 'ge_listing_node') {
	$ge_listing_node = TRUE;
} else {
	$listing_nid = ge_get_parent_exchange_node_id($node->nid);
	$ge_listing_node = FALSE;
}
// get id of original listing node
if (isset($node->field_parent_exchange_nid['und'][0]['value'])) {
	$field_parent_exchange_nid = $node->field_parent_exchange_nid['und'][0]['value'];
} else {
	$field_parent_exchange_nid = NULL;
}
// check if parent is listing node
$parent_is_listing_node = ge_is_parent_listing_node($field_parent_exchange_nid);
// get id of original listing node owner
if (isset($node->field_parent_exchange_uid['und'][0]['value'])) {
	$field_parent_exchange_uid = $node->field_parent_exchange_uid['und'][0]['value'];
} else {
	$field_parent_exchange_uid = NULL;
}

// get exchange type
$field_exchange_type = $node->field_exchange_type['und'][0]['value'];

// see if the item is available
if (isset($node->field_exchange_quantity['und'][0]['value'])) {
	$qty = $node->field_exchange_quantity['und'][0]['value'];
	if ($qty === 0) {
		$ge_availability = FALSE;
	} else {
		$ge_availability = TRUE;
	}
} else {
	$ge_availability = TRUE;
}
// see if the date has passed
$origin_field_exchange_longevity = $node->field_exchange_longevity[$node->language][0]['value'];
if ($origin_field_exchange_longevity == 'as_of_date') { // true = not an ongoing exchange
	if (time() <= strtotime($node->field_exchange_date[$node->language][0]['value2'])) {  // date has not passed
		$ge_availability = TRUE;
	} else {
		$ge_availability = FALSE;
	}
} else {
	$ge_availability = TRUE;
}



// get exchange state
if (isset($node->field_exchange_state['und'][0]['value'])) {
	$field_exchange_state = $node->field_exchange_state['und'][0]['value'];
} else {
	$field_exchange_state = NULL;
}
$field_exchange_status = $node->field_exchange_status['und'][0]['value'];
switch($field_exchange_status) {
	case "active":
		$allow_cancel = TRUE;
		$allow_comments = FALSE;
		break;
	case "inactive":
		$allow_cancel = FALSE;
		$allow_comments = FALSE;
		$ge_availability = FALSE; 
		break;
	case "pending_request":
		$allow_cancel = TRUE;
		$allow_comments = TRUE;
		break;
	case "pending_response":
		$allow_cancel = TRUE;
		$allow_comments = FALSE;
		break;
	case "accepted_request":
		$allow_cancel = TRUE;
		$allow_comments = TRUE;
		break;
	case "response_pending_activation":
		$allow_cancel = TRUE;
		$allow_comments = FALSE;
		break;
	case "request_pending_activation":
		$allow_cancel = TRUE;
		$allow_comments = FALSE;
		break;
	case "activated":
		$allow_cancel = FALSE;
		$allow_comments = TRUE;
		$allow_rating = TRUE;
		break;
	case "request_cancelled":
		$cancelled = TRUE;
		$allow_cancel = FALSE;
		$allow_comments = TRUE;
		$ge_availability = FALSE; 
		break;
	case "response_cancelled":
		$cancelled = TRUE;
		$allow_cancel = FALSE;
		$allow_comments = TRUE;
		$ge_availability = FALSE; 
		break;
	case "cancelled":
		$allow_cancel = FALSE;
		$allow_comments = TRUE;
		break;
	case "request_pending_completion":
		$allow_cancel = FALSE;
		$allow_comments = TRUE;
		break;
	case "response_pending_completion":
		$allow_cancel = FALSE;
		$allow_comments = TRUE;
		break;
	case "complete":
		$allow_cancel = FALSE;
		$allow_comments = TRUE;
		break;

}



// check if request has been accepted
$field_request_accepted = $node->field_request_accepted['und'][0]['value'];


global $user;
$logged_user = $user->uid;
$nid = $node->nid;
$node_owner = $node->uid;

if ($node->uid == $user->uid) {
	$owner = true;
} else {
	$owner = false;
}
// check is logged user owns parent exchange node
$logged_user_owns_parent_node = FALSE;
if ($field_parent_exchange_uid == $user->uid) {
	$logged_user_owns_parent_node = TRUE;
}

$field_give_or_share = $node->field_give_or_share['und'][0]['value'];
$skip_step_5 = FALSE;
switch($field_give_or_share) {
	case "gift":
		$is_gift = TRUE;
		$is_need = FALSE;
		$is_request = FALSE;
		
		break;
	case "gift_to_share":
		$is_gift = TRUE;
		$is_need = FALSE;
		$is_request = FALSE;
		break;
	case "wish":
		
		$is_gift = FALSE;
		$is_need = TRUE;
		$is_request = FALSE;
		break;
	case "wish_to_share":
		$is_gift = FALSE;
		$is_need = TRUE;
		$is_request = FALSE;
		break;
	case "response_for_gift":
		$is_gift = FALSE;
		$is_need = TRUE;
		$is_request = TRUE;
		break;
	case "response_for_gift_share":
		$is_gift = FALSE;
		$is_need = TRUE;
		$is_request = TRUE;
		break;
	case "response_for_wish":
		$is_gift = TRUE;
		$is_need = FALSE;
		$skip_step_5 = TRUE;
		$is_request = FALSE;
		break;
	case "response_for_wish_share":
		$is_gift = TRUE;
		$is_need = FALSE;
		$skip_step_5 = TRUE;
		$is_request = FALSE;
		break;
}
$anonymous_user = FALSE;
if ($user->uid == 0 ) {
	$anonymous_user = TRUE;
}
/*


gift|Gift to give away
gift_to_share|Gift to share & be returned
wish|Need to receive a gift
wish_to_share|Need to borrow & return
response_for_gift|Response for a gift that has been offered
response_for_gift_share|Response for a gift that has been offered for sharing
response_for_wish|Response for a need that has been offered
response_for_wish_share|Response for a need of sharing that has been offered
*/
// otherwise display based on status

// check access
if (!$owner) { // not owner ############################ display response action forms
	if ($ge_listing_node == FALSE){ // this is a response node 
		if ($field_parent_exchange_uid != $logged_user){ // check this person can rate the node
			$options['query'] = array(
				'node' =>$node->nid,
				'userid' => $user->uid,
			);
			if ($field_exchange_status != 'complete'){
				drupal_goto('access-denied', $options);
				//drupal_access_denied();
			
			}
			//drupal_goto('access-denied', $options);
		}
	}
}
?>
<?php //begin header block ================================================ ?>              
  <article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
	<div class="exchange gradient-box-top">&nbsp;</div>
  <div class="gradient-box content"<?php print $content_attributes; ?>>
  <?php if ((!$owner) and ($ge_listing_node == TRUE)) { ?>
 
  <?php
	  echo '<div id="flag-container" style="float:right;">';
	  $flag = flag_get_flag('needs_review');
	  if (!$flag->is_flagged($node->nid)) {
		  $count = $flag->get_count($node->nid);
		  if ($count > 0) {
			  if ($count == 1) {
				  $flag->flag_short = '&nbsp;+1';
			  		$flag->flag_long = 'This item has already been flagged '.$count.' time by another user.  If you agree, please add your own Flag.';
			  } else {
				  $flag->flag_short = '&nbsp;+1';
			  	$flag->flag_long = 'This item has already been flagged '.$count.' times by other users.  If you agree, please add your own Flag.';
			  }
			  
		  }
	  }
	  print flag_create_link('needs_review', $node->nid);
	  echo '</div>';
  } ?>
  <?php if ($ge_listing_node): ?>
  <div style="float:right; padding-right:30px; padding-top:2px">
  <?php // display tags
  if ($is_gift) {
	  echo '<div class="gift" style="display:inline-block">&nbsp;</div>';
  } else {
	  echo '<div class="wish" style="display:inline-block">&nbsp;</div>';
  }
  ?>
  
   <div class="<?php echo $field_exchange_type; ?>" style="display:inline-block">&nbsp;</div>
  </div><!-- /.gradient-box -->
  <?php endif; ?>
  
  <?php if (isset($node->field_exchange_photo['und'][0]['filename'])): ?>
     <?php print render($content['field_exchange_photo']); // display photo ?>
  <?php endif; ?>
 <header>
 
 <h1><?php print $title; ?></h1>
 <?php if (($owner) and ($ge_listing_node)): ?>
 <?php if ($is_gift) {
	 		echo '<a id="exchange_edit_btn" class="btn" href="/node/'.$nid.'/edit" style="float:right">Edit Gift</a>';
		 } else {
			echo '<a id="exchange_edit_btn" class="btn" href="/node/'.$nid.'/edit" style="float:right">Edit Need</a>';
		 }
       endif; ?>
    <?php if (!$ge_listing_node): ?>
    <!--<a href="/node/<?php echo $listing_nid; ?>" style="float:right">Original Listing</a>-->
    <?php $progress = ge_get_exchange_progress($node); // display progress meter on response nodes
		if (isset($progress[0])) {
			$display_video = FALSE;
			// display help videos
			if ($progress[0] == 'gift') { // gift responses have 8 steps
				if ($progress[1] == 'two') {
					echo '<div class="help-icon"><a class="youtube" href="#" rel="53dmDddixlU" title="Help Video: Respond to Gift Request with Offer"><img alt="Help Video: Respond to Gift Request with Offer" height="18" src="/'.drupal_get_path('theme','boron').'/images/help-icon.gif" width="71" /></a></div>';
					$display_video = TRUE;
				} elseif ($progress[1] == 'three') {
					echo '<div class="help-icon"><a class="youtube" href="#" rel="cYEA9Zprx90" title="Help Video: Accept Gift Offer"><img alt="Help Video: Accept Gift Offer" height="18" src="/'.drupal_get_path('theme','boron').'/images/help-icon.gif" width="71" /></a></div>';
					$display_video = TRUE;
				} elseif ($progress[1] == 'four') {
					echo '<div class="help-icon"><a class="youtube" href="#" rel="-1CxNSBvhNI" title="Help Video: Activate Gifting"><img alt="Help Video: Activate Gifting" height="18" src="/'.drupal_get_path('theme','boron').'/images/help-icon.gif" width="71" /></a></div>';
					$display_video = TRUE;
				} elseif ($progress[1] == 'six') {
					echo '<div class="help-icon"><a class="youtube" href="#" rel="TPhGsDnI7uk" title="Help Video: Finalize Gifting"><img alt="Help Video: Finalize Gifting" height="18" src="/'.drupal_get_path('theme','boron').'/images/help-icon.gif" width="71" /></a></div>';
					$display_video = TRUE;
				} elseif ($progress[1] == 'seven') {
					echo '<div class="help-icon"><a class="youtube" href="#" rel="TPhGsDnI7uk" title="Help Video: Finalize Gifting"><img alt="Help Video: Finalize Gifting" height="18" src="/'.drupal_get_path('theme','boron').'/images/help-icon.gif" width="71" /></a></div>';
					$display_video = TRUE;
				} elseif ($progress[1] == 'eight') {
					echo '<div class="help-icon"><a class="youtube" href="#" rel="TPhGsDnI7uk" title="Help Video: Finalize Gifting"><img alt="Help Video: Finalize Gifting" height="18" src="/'.drupal_get_path('theme','boron').'/images/help-icon.gif" width="71" /></a></div>';
					$display_video = TRUE;
				}
			} else {
				if ($progress[1] == 'two') { // need response seven steps
					echo '<div class="help-icon"><a class="youtube" href="#" rel="cYEA9Zprx90" title="Help Video: Accept Gift Offer"><img alt="Help Video: Accept Gift Offer" height="18" src="/'.drupal_get_path('theme','boron').'/images/help-icon.gif" width="71" /></a></div>';
					$display_video = TRUE;
				} elseif ($progress[1] == 'three') {
					echo '<div class="help-icon"><a class="youtube" href="#" rel="-1CxNSBvhNI" title="Help Video: Activate Gifting"><img alt="Help Video: Activate Gifting" height="18" src="/'.drupal_get_path('theme','boron').'/images/help-icon.gif" width="71" /></a></div>';
					$display_video = TRUE;
				} elseif ($progress[1] == 'five') {
					echo '<div class="help-icon"><a class="youtube" href="#" rel="TPhGsDnI7uk" title="Help Video: Finalize Gifting"><img alt="Help Video: Finalize Gifting" height="18" src="/'.drupal_get_path('theme','boron').'/images/help-icon.gif" width="71" /></a></div>';
					$display_video = TRUE;
				} elseif ($progress[1] == 'six') {
					echo '<div class="help-icon"><a class="youtube" href="#" rel="TPhGsDnI7uk" title="Help Video: Finalize Gifting"><img alt="Help Video: Finalize Gifting" height="18" src="/'.drupal_get_path('theme','boron').'/images/help-icon.gif" width="71" /></a></div>';
					$display_video = TRUE;
				} elseif ($progress[1] == 'seven') {
					echo '<div class="help-icon"><a class="youtube" href="#" rel="TPhGsDnI7uk" title="Help Video: Finalize Gifting"><img alt="Help Video: Finalize Gifting" height="18" src="/'.drupal_get_path('theme','boron').'/images/help-icon.gif" width="71" /></a></div>';
					$display_video = TRUE;
				}
			}
			if ($display_video == TRUE) { // javascript handler for youtube popoup
				echo '<script type="text/javascript">
							jQuery(function () {
								jQuery("a.youtube").YouTubePopup({ autoplay: 1, draggable: true, modal: false, });
							});
						</script>';
			}
			echo '<div class="progress-meter-container">';
			if ($progress[0] == 'gift') {
				$linkurl = '/help/gift-progress-meter';
			} else {
				$linkurl = '/help/need-progress-meter';
			}
			
			echo '<strong>Progress Meter</strong><br />';
			echo '<a class="simple-dialog progress-meter '.$progress[0].' '.$progress[1].'" href="'.$linkurl.'" rel="width:950;resizable:false;position:[center,60]" name="block-system-main" title="Progress Meter">&nbsp;</a>';
			echo '</div>';
		} ?>
	<?php endif; ?>
    <?php // get user photo is exists
	$node_owner_data = user_load( $node_owner );
		if (isset($node_owner_data->picture->filename)) {
			$userbgimage = 'style="padding-left:30px; background-image:url(/sites/thegiftingearth.net/files/styles/user_thumbnail/public/pictures/'.$node_owner_data->picture->filename.');"';
		} else {
			$userbgimage = '';
		}
	?>
    <div class="bluebox">
    <p style="margin-top:0px;">
    	<span class="label">User/Rating:</span>
    	<a class="userlink" href="/user/<?php echo $node_owner; ?>" <?php echo $userbgimage; ?>>
    		<?php echo $node_owner_data->field_user_firstname['und'][0]['safe_value']; ?>&nbsp;<?php echo $node_owner_data->field_user_lastname['und'][0]['safe_value']; ?>&nbsp;<?php
    	$number_of_exchanges = 0;
    	if (!empty($node_owner_data->field_user_number_of_exchanges['und'][0]['value'])) {
    		$number_of_exchanges = $node_owner_data->field_user_number_of_exchanges['und'][0]['value'];
    	} ?>(<?php echo $number_of_exchanges; ?>)</a> <?php 
	if (ge_convert_stars_to_classes(ge_get_user_rating($node_owner)) != '') {
				echo '<span class="display-stars '.ge_convert_stars_to_classes(ge_get_user_rating($node_owner)).'" style="">&nbsp;</span>';
			} else {
				echo '';
			} ?> <br /> 
<?php if ($ge_listing_node): ?>			
	<span class="label">Location:</span><a class="mapicon" href="https://maps.google.com/?q=<?php echo $node->locations[0]['city']; ?>+<?php echo $node->locations[0]['province']; ?><?php //echo $node->locations[0]['postal_code']; ?>" rel="external"><?php echo $node->locations[0]['city']; ?>, <?php echo $node->locations[0]['province']; ?></a><br /> 
 <?php endif;
 	 if ($ge_listing_node): 
	 echo '<span class="label">Date Posted:</span>';
 else: 
 	echo '<span class="label">Date of Response:</span>';
 endif; 
      echo date('M. j, Y', strtotime($node->field_exchange_date['und'][0]['value'])); ?><br /> 
    <?php 
	if (isset($node->field_exchange_date['und'][0]['value2'])) {
		$date1 = date('M. j, Y', strtotime($node->field_exchange_date['und'][0]['value']));
		$date2 = date('M. j, Y', strtotime($node->field_exchange_date['und'][0]['value2']));
		
		if ($date1 != $date2) {
			echo '<span class="label">Available Until:</span>'.date('M. j, Y', strtotime($node->field_exchange_date['und'][0]['value2'])).'<br />';
		}
	} ?>

	<span class="label">Gift or Share:</span><?php if (!$ge_listing_node): // link to original on responses ?><a href="/node/<?php echo $listing_nid; ?>"><?php echo $content['field_give_or_share'][0]['#markup']; ?></a><br />
    <?php else: 
		echo $content['field_give_or_share'][0]['#markup']; ?><br />
    <?php endif; ?> 
    <?php if ($ge_listing_node): 
		if ($is_gift) { ?>
        <span class="label">Number Available:</span><?php echo $node->field_exchange_quantity['und'][0]['value']; ?><br />
        <?php if ($node->field_exchange_longevity['und'][0]['value'] == 'on_going'):
            echo '<span class="label">As of Date:</span>'.$content['field_exchange_longevity'][0]['#markup'];
         endif; 
		} ?>
     <?php endif; ?>
    </p>
    <?php 
	$subject = str_replace('"', "", $title);
	$subject = str_replace("'", "", $subject);
?>
	<?php if ($ge_listing_node): // display link to ask giftor a question ?>
    <?php if (!$anonymous_user) { ?>
    <p><a href="/messages/new/<?php echo $node_owner; ?>?destination=node/<?php echo $nid; ?>&subject=<?php echo urlencode($subject); ?>"><?php 
	echo 'Ask '.$node_owner_data->field_user_firstname['und'][0]['safe_value'].' '.$node_owner_data->field_user_lastname['und'][0]['safe_value'].' a question?';
	?></a></p>
    <?php } ?>
    
           
    <?php else: // not a listing?>
    
    <?php endif; ?>
    <div style="clear:both"></div>
    </div><!-- /bluebox -->
    </header>
    <div class="content">
    <?php if ($ge_listing_node): ?>
    <p><!--<span class="label" style="padding-bottom:10px; float:left;">Description:</span><br />--><?php echo $node->field_detailed_exchange_desc['und'][0]['safe_value']; ?></p>
    <?php endif; ?>
                
                
     <?php //end header block ================================================ ?> 
   
    <?php 
	if (!$owner): // not owner ############################ display response action forms
	
		if ($ge_listing_node == FALSE): // this is a response node 
			if ($field_parent_exchange_uid == $logged_user): // check this person can rate the node ?>
				
            	<?php if ($field_exchange_status == 'pending_request'): ?>
                <div id="comment-form-wrapper">
					<h2 class="title"><?php print t('Include optional message for'); ?> <em><?php echo $node_owner_data->field_user_firstname['und'][0]['safe_value'].' '.$node_owner_data->field_user_lastname['und'][0]['safe_value']; ?></em></h2>
					<p><?php print t('The form below will send a message to'); ?> <?php echo $node_owner_data->field_user_firstname['und'][0]['safe_value'].' '.$node_owner_data->field_user_lastname['und'][0]['safe_value']; ?>. <strong><?php print t('This message will be publicly visible after the gifting process is concluded.'); ?></strong> <?php 
					print l(t('Click here to send a private message'), 'messages/new/'.$node->uid, array('query' => array('destination' => 'node/'.$node->nid, 'subject' => urlencode($node->title))), array('attributes' => array('class' => array('private-message-link')))); ?>.<br />
                            
                <?php // variables for form
                 $form_variables['exchange_action_form_exchange_status'] = $node->field_exchange_status[$node->language][0]['value'];
                 $form_variables['exchange_action_form_nid'] = $nid;
				 $form_variables['exchange_action_form_skip_step_5'] = $skip_step_5;
				 $form_variables['exchange_action_form_logged_user'] = $logged_user;
				 $form_variables['exchange_action_form_uid'] = $node->uid;
				 $form_variables['is_gift'] = $is_gift;
                 print drupal_render(drupal_get_form('exchange_action_form', $form_variables)); ?>
                </div><!-- /#comment-form-wrapper-->
                 <?php elseif ($field_exchange_status == 'pending_response'): // display accept exchange form ?>
                    	<?php if ($field_giftingearth_node_subtype == 'ge_original_response_node'): ?>
                        		<div id="comment-form-wrapper">
                                    <h2 class="title"><?php print t('Include optional message for'); ?> <em><?php echo $node_owner_data->field_user_firstname['und'][0]['safe_value'].' '.$node_owner_data->field_user_lastname['und'][0]['safe_value']; ?></em></h2>
                                    <p><?php print t('The form below will send a message to'); ?> <?php echo $node_owner_data->field_user_firstname['und'][0]['safe_value'].' '.$node_owner_data->field_user_lastname['und'][0]['safe_value']; ?>. <strong><?php print t('This message will be publicly visible after the gifting process is concluded.'); ?></strong> <?php 
                                    print l(t('Click here to send a private message'), 'messages/new/'.$node->uid, array('query' => array('destination' => 'node/'.$node->nid, 'subject' => urlencode($node->title))), array('attributes' => array('class' => array('private-message-link')))); ?>.<br />
                                            
                                <?php // variables for form
                                 $form_variables['exchange_accept_form_nid'] = $nid;
                                 $form_variables['exchange_accept_form_parent_exchange_nid'] = $field_parent_exchange_nid;
                           		 $form_variables['is_gift'] = $is_gift;
                                 print drupal_render(drupal_get_form('exchange_accept_form', $form_variables)); ?>
                            </div><!-- /#comment-form-wrapper-->
                   <?php else: ?>
                      <?php // variables for form
						 $form_variables['exchange_action_form_exchange_status'] = $node->field_exchange_status[$node->language][0]['value'];
						 $form_variables['exchange_action_form_nid'] = $nid;
						 $form_variables['exchange_action_form_skip_step_5'] = $skip_step_5;
						 $form_variables['exchange_action_form_logged_user'] = $logged_user;
						 $form_variables['exchange_action_form_uid'] = $node->uid;
						 $form_variables['is_gift'] = $is_gift;
						 print drupal_render(drupal_get_form('exchange_action_form', $form_variables)); ?>
                    <?php endif; ?>
                    <?php //elseif ($field_exchange_status == 'accepted'): //  ?>
                    
					<?php elseif ($field_exchange_status == 'response_pending_activation'): // display activation form ?>
                    	<div id="comment-form-wrapper">
	                        <h2 class="title"><?php print t('Private contact instructions for'); ?> <em><?php echo $node_owner_data->field_user_firstname['und'][0]['safe_value'].' '.$node_owner_data->field_user_lastname['und'][0]['safe_value']; ?></em></h2>
	                        <?php print t('<p>Please advise your gifting partner how you will contact them. This message is private <strong>will not be published on the site</strong>.'); ?>:<br />
	                        <?php 
	                        // variables for form
	                        $form_variables['nid'] = $nid;
	                        $form_variables['field_parent_exchange_nid'] = $field_parent_exchange_nid;
	                        print drupal_render(drupal_get_form('exchange_activate_form', $form_variables)); ?>
                        </div><!-- /#comment-form-wrapper-->
                   <?php elseif ($field_exchange_status == 'activated'): // ?>
                    
                    <div id="comment-form-wrapper">
	                    <?php 
	                        // variables for form
	                        $form_variables['exchange_complete_form_nid'] = $nid;
	                        $form_variables['exchange_complete_parent_exchange_nid'] = $field_parent_exchange_nid;
	                        print drupal_render(drupal_get_form('exchange_complete_form', $form_variables)); ?>
                    </div><!-- /#comment-form-wrapper-->
                	
                    <?php elseif (($field_exchange_status == 'cancelled') or ($field_exchange_status == 'request_cancelled') or ($field_exchange_status == 'response_cancelled')): //cancelled ?>
                    <p class="marker">The gifting process has been cancelled</p>
                    
                        
                     
                    <?php elseif (($field_exchange_status == 'complete') or ($field_exchange_status == 'response_pending_completion')):  
						if ($field_exchange_status == 'complete') {
							$show_completequest = FALSE;
							$show_completecontainer = TRUE;
						} else {
							$show_completequest = FALSE;
							$show_completecontainer = TRUE;
						}
						?>
                    
                    <div id="completequest" <?php if($show_completequest == FALSE) { echo 'style="display:none" '; } ?>>
                       <p><strong><?php print t('Was gifting completed?'); ?></strong></p>
                          <p><a id="completetrue" class="btn" href="javascript:;">Yes</a> <a id="completefalse" class="btn" href="javascript:;">No</a></p>
                    </div><!-- /#completequest -->
                <div id="completecontainer" <?php if($show_completecontainer == FALSE) { echo 'style="display:none" '; } ?>>
                    <div id="comment-form-wrapper">
                    	<div class="misccontainer" style="clear:both; float:right; padding-top:5px">
                            <p><strong><?php print t('Please thoughtfully rate your gifting partner by each criteria below.'); ?> <span class="form-required" title="This field is required.">*</span></strong></p>   
                            <div style="float:right">
                            <?php if ($is_gift) { ?>
                            	<a class="simple-dialog textlink" href="/help/rating-system-overview" rel="width:950;resizable:false;position:[center,60]" name="giftor-criteria" title="Rating System Overview">Rating System Overview</a>
                                <?php } else { ?>
                                <a class="simple-dialog textlink" href="/help/rating-system-overview" rel="width:950;resizable:false;position:[center,60]" name="giftee-criteria" title="Rating System Overview">Rating System Overview</a>
                                <?php } ?>
                            </div>
                            <?php print render($content['field_exchange_rating_com']);   ?>
                            <?php if ($is_gift) {
								$content['field_exchange_rating_accuracy']['#title'] = 'Generosity'; 
							} else {
								$content['field_exchange_rating_accuracy']['#title'] = 'Gratitude'; 
							}?>
                            <?php print render($content['field_exchange_rating_accuracy']); ?>
                            <?php print render($content['field_exchange_rating_response']);   ?>
                            </div><!-- /.misccontainer-->
                            <?php 
							// set checkbox defaults
						if ($field_request_accepted == 1) {
							$delivered = TRUE;
						} elseif ($field_request_accepted == 0) {
							$delivered = FALSE;
						} else {
							$delivered = 3; // set default to blank (neither checked)
						}
						
                        // variables for form
                        $form_variables['exchange_rate_form_nid'] = $nid;
                        $form_variables['exchange_rate_parent_exchange_nid'] = $field_parent_exchange_nid;
						$form_variables['exchange_rate_parent_exchange_uid'] = $node->uid;
						$form_variables['field_exchange_status'] = $field_exchange_status;
						$form_variables['delivered'] = $delivered;
                        print drupal_render(drupal_get_form('exchange_rate_form', $form_variables)); ?>
                   	
                    </div><!-- /#completecontainer-->
                  </div><!-- /#completecontainer-->
                               
                                   
                       
                    <?php else: ?>
                      
                    <?php endif;  ?>  
                   
                   <?php if (($allow_cancel == TRUE) and ($field_exchange_status != 'pending_request')): ?>
                    <?php // cancel form
						// variables for form
                        $form_variables['exchange_cancel_form_nid'] = $nid;
                        $form_variables['exchange_cancel_form_parent_exchange_nid'] = $field_parent_exchange_nid;
						$form_variables['field_giftingearth_node_subtype'] = $field_giftingearth_node_subtype;
						$form_variables['is_gift'] = false;
						$form_variables['is_request'] = false;
						$form_variables['status'] = 'unknown';
                        print drupal_render(drupal_get_form('exchange_cancel_form', $form_variables)); ?>
                   <?php endif; ?>
                <?php else: 
				// this is what other logged in users can see 
				
			?>
				<div id="feedbackarea" style="clear:both;">
                        <h2>Gifting Feedback</h2>
<div class="misccontainer" style="float:right; padding-left:30px; width:400px">  
                        		<p><strong>Rating</strong></p>  
                                <div>
                               <div class="field-label" style="float:left;">Communication:&nbsp;</div><div class="display-stars <?php echo ge_convert_stars_to_classes($node->field_exchange_rating_com['und'][0]['average']); ?>">&nbsp;</div>
                               </div>
                               <div><div class="field-label" style="float:left;"><?php if ($is_gift) : ?>
                              	Generosity:
                                <?php else: ?>
                                Gratitude:
                                <?php endif; ?>&nbsp;</div><div class="display-stars <?php echo ge_convert_stars_to_classes($node->field_exchange_rating_accuracy['und'][0]['average']); ?>">&nbsp;</div>
                               </div>
                               
                               <div>
                               <div class="field-label" style="float:left;">Co-operation:&nbsp;</div><div class="display-stars <?php echo ge_convert_stars_to_classes($node->field_exchange_rating_response['und'][0]['average']); ?>">&nbsp;</div>
                               </div>
                               <p><?php if ($is_gift) { ?>
                            	<a class="simple-dialog textlink" href="/help/rating-system-overview" rel="width:950;resizable:false;position:[center,60]" name="giftor-criteria" title="Rating System Overview">Rating System Overview</a>
                                <?php } else { ?>
                                <a class="simple-dialog textlink" href="/help/rating-system-overview" rel="width:950;resizable:false;position:[center,60]" name="giftee-criteria" title="Rating System Overview">Rating System Overview</a>
                                <?php } ?></p>
                      </div><!-- /.misccontainer-->
                      
                      <p><strong><?php if ($field_request_accepted == 1) { 
								echo 'YES, gift was successfully delivered.';
							} else {
								echo 'NO, gift was not successfully delivered.'; 
							} ?></strong></p>
                            
                            <p><a href="/node/<?php echo ge_get_partner_exchange_node_id($node->nid); ?>">View Partner record for this gifting</a></p>
                   </div><!-- #feedbackarea-->
                   <div style="clear:both;">&nbsp;</div>
                   <?php
					  // hide the comments and links
					  $content['links']['comment']['#access'] = FALSE;
					  foreach ($content['comments']['comments'] as $key => $value) {
						  if (is_numeric($key)) {
							  unset($content['comments']['comments'][$key]['links']['comment']['#links']['comment-reply']);
						  }
					  }
					  $content['comments']['comment_form']['#access'] = FALSE;
					 $comments = render($content['comments']); 
					 $comments = str_replace('<h2 class="title">Add message for Giftor</h2>','',$comments);
					 echo $comments; ?>
          		<?php endif;  ?>
      	
		<?php else: // this is a listing node ?>
            		<?php if ($field_exchange_status == 'active'): ?>

                    <?php if ($hide_ge_response_buttons == FALSE) : ?>
                    
                        <form id="exchange_action_form" name="exchange_action_form" action="" method="post" style="clear:both">
                        <div id="comment-form-wrapper">
                        	<?php 
							if ($anonymous_user) { ?>
								<p>You must <a href="/user">log in</a> to respond to this posting</p>
<?php } else {
							?>
							<h2 class="title"><?php print t('Include optional message for'); ?> <em><?php echo $node_owner_data->field_user_firstname['und'][0]['safe_value'].' '.$node_owner_data->field_user_lastname['und'][0]['safe_value']; ?></em></h2>
							<?php if ($is_gift): ?>
                            	<p><?php print t('What use would you make of this gift? Why is this gift of special value to you? How might this gift help you? Gifting is not neutral and anonymous. It is personal and caring'); ?>.</p>
                            <?php else: ?>
                            	<p><?php print t('Why are you offering this gift? Gifting is not neutral and anonymous. It is personal and caring'); ?>.</p>
                            <?php endif; ?>
                            	<p><?php print t('The form below will send a message to'); ?> <?php echo $node_owner_data->field_user_firstname['und'][0]['safe_value'].' '.$node_owner_data->field_user_lastname['und'][0]['safe_value']; ?>. <strong><?php print t('This message will be publicly visible after the gifting process is concluded.'); ?></strong> <?php 
								print l(t('Click here to send a private message'), 'messages/new/'.$node->uid, array('query' => array('destination' => 'node/'.$node->nid, 'subject' => urlencode($node->title))), array('attributes' => array('class' => array('private-message-link')))); ?>.<br />
                    		
							
                            
                          <textarea id="initial_question" name="exchange_action_form_initial_question" cols="60" rows="2" style="width:675px"></textarea></p>
                            
                            <div><input type="hidden" name="exchange_action_form_exchange_status" value="<?php echo $node->field_exchange_status[$node->language][0]['value']; ?>" />
                              <input type="hidden" name="exchange_action_form_nid" value="<?php echo $nid; ?>" />
                              <input type="hidden" name="exchange_action_form_logged_user" value="<?php echo $logged_user; ?>" />
                              <input type="hidden" name="exchange_action_form_uid" value="<?php echo $node->uid; ?>" />
                              </div>
                                <p><input id="ge_submit" class="form-submit <?php if ($is_gift) { echo 'gift'; } else { echo 'need'; } ?>" type="submit" <?php if ($is_gift) { echo 'value="Request Gift"'; } else { echo 'value="Offer Gift"'; } ?> /></p>
                             </div><!-- #comment-form-wrapper-->
                        </form>
                        <?php } ?>
                        <?php else: ?>
                        <div id="comment-form-wrapper">
							<?php if ($anonymous_user): ?>
                            	<p>You must <a href="/user">log in</a> to respond to this posting</p>
                            <?php else: ?>
                                <h3><?php print t("Response Temporarily Closed.") ?></h3>
                                <p><?php print t("Gift and Need responses are temporarily closed, please check back later.") ?></p>
                            <?php endif; ?>
                        </div><!-- #comment-form-wrapper-->
                       <?php endif; ?>
                    <?php elseif ($field_exchange_status == 'pending'): // display accept exchange form ?>
                    
                    <?php elseif ($field_exchange_status == 'accepted'): //  ?>
                       
                    <?php elseif ($field_exchange_status == 'pending_activation'): // display activation form ?>
                	
                    <?php elseif ($field_exchange_status == 'activated'): //m ?>
                    
					<?php elseif (($field_exchange_status == 'cancelled') or ($field_exchange_status == 'request_cancelled') or ($field_exchange_status == 'response_cancelled')): //cancelled ?>
                      <p class="marker">The item has been cancelled</p>
                      
                    <?php elseif ($field_exchange_status == 'complete'): //m ?>
                    
                    <?php endif;  ?>  
 
	
		<?php endif;
	else: // this is the node owner ?>
    
    <?php if ($field_exchange_status == 'inactive'): //m ?>
          <p class="marker">This item has been cancelled</p>
     <?php endif; ?>
    	<?php if ($ge_listing_node == FALSE): // this is a response node ?>
        			<?php if ($field_exchange_status == 'active'): ?>
                       
                    <?php elseif ($field_exchange_status == 'pending'): // ?>
                       
                    
                    <?php elseif ($field_exchange_status == 'accepted'): //  ?>
                       
                    <?php elseif ($field_exchange_status == 'pending_activation'):  ?>
                	
                    
                    <?php elseif ($field_exchange_status == 'activated'): //m ?>
                    <p>Gifting activated, and can no longer be cancelled</p>
                    	
                	
                    <?php elseif (($field_exchange_status == 'cancelled') or ($field_exchange_status == 'request_cancelled') or ($field_exchange_status == 'response_cancelled')): //cancelled ?>
                     <p class="marker">The gifting process has been cancelled</p>
                     
                    <?php elseif ($field_exchange_status == 'complete'): //m ?>
                    
                    <div id="feedbackarea" style="clear:both;">
                        <h2>Gifting Feedback</h2>
							<div style="float:right; padding-left:30px; width:400px">  
                        		<p><strong>Rating</strong></p>   
                                <div>
                               		<div class="field-label" style="float:left;">Communication:&nbsp;</div><div class="display-stars <?php echo ge_convert_stars_to_classes($node->field_exchange_rating_com['und'][0]['average']); ?>">&nbsp;</div>
                               	</div>
	                              <div><div class="field-label" style="float:left;">
	                              <?php if ($is_gift) : ?>
	                              	Generosity:
	                                <?php else: ?>
	                                Gratitude:
	                                <?php endif; ?>&nbsp;</div><div class="display-stars <?php echo ge_convert_stars_to_classes($node->field_exchange_rating_accuracy['und'][0]['average']); ?>">&nbsp;</div>
	                               </div>
                               
	                               <div>
	                               <div class="field-label" style="float:left;">Co-operation:&nbsp;</div><div class="display-stars <?php echo ge_convert_stars_to_classes($node->field_exchange_rating_response['und'][0]['average']); ?>">&nbsp;</div>
	                               </div>
	                               <p><?php if ($is_gift) { ?>
	                            	<a class="simple-dialog textlink" href="/help/rating-system-overview" rel="width:950;resizable:false;position:[center,60]" name="giftor-criteria" title="Rating System Overview">Rating System Overview</a>
	                                <?php } else { ?>
	                                <a class="simple-dialog textlink" href="/help/rating-system-overview" rel="width:950;resizable:false;position:[center,60]" name="giftee-criteria" title="Rating System Overview">Rating System Overview</a>
	                                <?php } ?></p>
                      		</div>
                      
                      <p>Was gift delivered?<br /><strong><?php if ($field_request_accepted == 1) { 
								echo 'YES, gift was successfully delivered.';
							} else {
								echo 'NO, gift was not successfully delivered.'; 
							} ?></strong></p>
                            <p><em>Disagree? Send a <a href="/messages/new/<?php echo $field_parent_exchange_uid; ?>">private message to your partner</a> to attempt to resolve the issue. If that fails, you may <a href="/contact">contact a site administrator</a> to assist in resolving the issue.</em></p>
                            <?php
							if (ge_is_parent_listing_node($field_parent_exchange_nid)) {
								// locate other exhange nid
								$ex_nid = ge_get_partner_exchange_node_id($node->nid);
							} else {
								$ex_nid = $field_parent_exchange_nid;
							}
							?>
                            <p><a class="btn" href="/node/<?php echo $ex_nid; ?>">Don't forget to rate your partner.</a></p>
                       
                       
                   </div><!-- #feedbackarea-->
                    <?php endif;  ?>
                    
    		<?php if ($allow_cancel == TRUE): ?>
                       <?php // cancel form
						// variables for form
                        $form_variables['exchange_cancel_form_nid'] = $nid;
                        $form_variables['exchange_cancel_form_parent_exchange_nid'] = $field_parent_exchange_nid;
						$form_variables['field_giftingearth_node_subtype'] = $field_giftingearth_node_subtype;
						$form_variables['is_gift'] = $is_gift;
						if (isset($is_request)) {
							$form_variables['is_request'] = $is_request;
						} else {
							$form_variables['is_request'] = false;
						}
						$form_variables['status'] = 'known';
                        print drupal_render(drupal_get_form('exchange_cancel_form', $form_variables)); ?>
                   <!--<form id="exchange_cancel_form" name="exchange_cancel_form" action="" method="post">
                      <div><input type="hidden" name="exchange_cancel_form_nid" value="<?php echo $nid; ?>" />
                      <input type="hidden" name="exchange_cancel_form_parent_exchange_nid" value="<?php echo $field_parent_exchange_nid; ?>" />
                      <input type="hidden" name="field_giftingearth_node_subtype" value="<?php echo $field_giftingearth_node_subtype; ?>" />
                      </div>
                      <?php if (($is_gift) and ($field_giftingearth_node_subtype == 'ge_original_response_node')): 
					  if($is_request == TRUE) {
							$canceltext = "Cancel Request";
						} else {
							$canceltext = "Cancel Offer";
						}?>
                        <div style="float:right; clear:right; z-index:999">
                        <input id="cancelbutton" class="plain_btn" type="submit" value="<?php echo $canceltext; ?>" onclick="return confirmCancel()" /> </div>
                        <div id="canceldialog" style="display:none; clear:right">
                            <strong>Include reason for cancelling (optional):</strong><br />
                                <textarea id="exchange_cancel_form_reason" name="exchange_cancel_form_reason" cols="60" rows="2" style="width:675px"></textarea>
                        </div>
                        <?php else: 
						if($is_request == TRUE) {
							$canceltext = "Cancel Request";
						} else {
							$canceltext = "Cancel Offer";
						}
						?>
                        <div style="float:right"><input id="cancelbutton" class="plain_btn" type="submit" value="<?php echo $canceltext; ?>" onclick="return confirmCancel()" /> </div>
                        <div id="canceldialog" style="display:none; clear:right">
                            <strong>Include reason for cancelling (optional):</strong><br />
                                <textarea id="exchange_cancel_form_reason" name="exchange_cancel_form_reason" cols="60" rows="2" style="width:675px"></textarea>
                        </div>
                        <?php endif; ?>
                       
      				</form>  -->
                    <?php endif; ?> 
            
           
            
		<?php else:// listing node & is owner ?>
          <?php if ($allow_cancel == TRUE): //cancellation for on listing nodes ?>
                       
                   <form id="exchange_cancel_form" name="exchange_cancel_form" action="" method="post">
                      <div><input type="hidden" name="exchange_cancel_form_nid" value="<?php echo $nid; ?>" />
                      <input type="hidden" name="field_giftingearth_node_subtype" value="<?php echo $field_giftingearth_node_subtype; ?>" />
                      </div>
                      <div style="float:right"><input class="plain_btn" type="submit" value="Cancel Listing" onclick="return confirmCancel()" /> </div>
                      
                       
      				</form>  
                    <?php endif; ?> 
	 		
        
       <?php endif; // end is listing node?>
	<?php endif; // end this is the node owner ?>
    
    </div>
  <?php if (($owner) or ($logged_user_owns_parent_node)): // allow comments ?>
    <div style="clear:both;">&nbsp;</div>
        <?php if ($ge_listing_node == FALSE) {
			print render($content['comments']); 
		} ?>	
    <?php endif; ?>
     

      
  <?php if (!empty($content['field_tags']) || !empty($content['links'])): ?>
    <footer>
      <?php print render($content['field_tags']); ?>
      <?php print render($content['links']); ?>
    </footer>
  <?php endif; ?>
  
    <div style="clear:both">&nbsp;</div>
  </div> <!-- /.content -->
  <div class="exchange gradient-box-bottom">&nbsp;</div>


</article> <!-- /.node -->