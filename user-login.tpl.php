<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 */
// set tab index
$form['name']['#attributes']['tabindex'] = '1';
$form['pass']['#attributes']['tabindex'] = '2';
?>

<div id="subscribe-form-container">
  <div class="home-errors" id="edit-name-error-msg">Please enter your username</div>
  <div class="home-errors" id="edit-pass-error-msg">Please enter your password <span class="passlink"><a href="/user/password">Forgot password?</a></span></div>
   <?php print drupal_render($form['name']); ?>
    Are you new to the Gifting Earth?<br />
    <input type="checkbox" id="account_checkbox" name="account_checkbox" value="true"> Yes, create a new account<br />
    <?php print drupal_render($form['pass']); ?>
<?php
    print drupal_render($form['form_build_id']);
    print drupal_render($form['form_id']);
    print render($form['captcha']);
    print drupal_render($form['actions']);
?>
</div><!-- /# subscribe-form-container-->


    


