<?php

/**
 * @file
 * Default theme implementation to display the basic html structure of a single
 * Drupal page.
 *
 * Variables:
 * - $css: An array of CSS files for the current page.
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or 'rtl'.
 * - $rdf_namespaces: All the RDF namespace prefixes used in the HTML document.
 * - $grddl_profile: A GRDDL profile allowing agents to extract the RDF data.
 * - $head_title: A modified version of the page title, for use in the TITLE tag.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $page_top: Initial markup from any modules that have altered the
 *   page. This variable should always be output first, before all other dynamic
 *   content.
 * - $page: The rendered page content.
 * - $page_bottom: Final closing markup from any modules that have altered the
 *   page. This variable should always be output last, after all other dynamic
 *   content.
 * - $classes String of classes that can be used to style contextually through
 *   CSS.
 *
 * @see template_preprocess()
 * @see template_preprocess_html()
 * @see template_process()
 */
 // check if this is the map node
 $showmap = false;
 if (arg(0) == 'node') {
	  if (arg(1) == 25) { // 4 = local, 25 = production
		  $showmap = true;
	  }
 }
?><?php print $doctype; ?>
<html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>" <?php print $rdf->version . $rdf->namespaces; ?>>
<head<?php print $rdf->profile; ?>>

  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <meta name="viewport" content="user-scalable = yes">
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <?php if ($showmap) :  ?>
  <script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script> 
<script type="text/javascript" src="/<?php print drupal_get_path('theme','boron'); ?>/scripts/downloadxml.js"></script>
<script type="text/javascript"> 
//<![CDATA[
      // arrays to hold copies of the markers
      var gmarkers = []; 
     // global "map" variable
      var map = null;

// A function to create the marker and set up the event window function 
function createMarker(latlng, name, html, type) {
	themePath = <?php print '\'/' . drupal_get_path('theme','boron') . '\''; ?>;
	if (type == 'gift') {
		var iconImage = themePath + '/images/marker-green.png';
	} else {
		if (type == 'gift_to_share') {
			var iconImage = themePath + '/images/marker-green.png';
		} else {
			var iconImage = themePath + '/images/marker-blue.png';
		}
	}
    var contentString = html;
    var marker = new google.maps.Marker({
        position: latlng,
        map: map,
		icon: iconImage,
        zIndex: Math.round(latlng.lat()*-100000)<<5
		
        });

    google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(contentString); 
        infowindow.open(map,marker);
        });
    gmarkers.push(marker);
}
 
// This function picks up the click and opens the corresponding info window
function myclick(i) {
  google.maps.event.trigger(gmarkers[i], "click");
}
function checkCoords(lat,lng,previousLat,previousLng) {
	if (previousLat == lat) {
		if (previousLng == lng) {
			// these are at the same point
			return true;
		} else {
			// different point
			return false;
		} 
	} else {
		// different point
		return false;
	}
}
<?php // get logged user data
global $user;
if ($user->uid != 0) {
	$userdata = user_load($user->uid);
	if (!empty($userdata->location['latitude'])) {
		$lat = $userdata->location['latitude'];
		$lng = $userdata->location['longitude'];
	}
} 
// default values
if (empty($lat)) {
	$lat = 36.97842095659727;
	$lng = -121.8988037109375;
}
?>

function initialize() {
	// load markers initially with defaults
	  if (document.getElementById("lat-center").value == '') {
		  var lat = <?php echo $lat; ?>;
	  } else {
		  var lat = document.getElementById("lat-center").value;
	  }
	  if (document.getElementById("lng-center").value == '') {
		  var lng = <?php echo $lng; ?>;
	  } else {
		  var lng = document.getElementById("lng-center").value;
	  }
	  if (document.getElementById("current-zoom").value == '') {
		  var zoom = 9;
	  } else {
		  var zoom = parseFloat(document.getElementById("current-zoom").value);
	  }
  // create the map
  var myOptions = {
    zoom: parseFloat(zoom),
    center: new google.maps.LatLng(lat,lng),
    mapTypeControl: true,
    mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
    navigationControl: true,
	scaleControl: true,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  map = new google.maps.Map(document.getElementById("map_canvas"),
                                myOptions);
 
  google.maps.event.addListener(map, 'click', function() {
        infowindow.close();
  });
  
  
  function loadPoints(map,lat,lng,zoom) {
	   // get checkbox status 
	   var showNeeds = true;
		var showGifts = true;
		var queryString = '';
		var radius = 10000;
		if (document.getElementById("showneed").checked == false) {
			showNeeds = false;
		}
		if (document.getElementById("showgift").checked == false) {
			showGifts = false;
		}
		if (showNeeds == true) {
			if (showGifts == true ) {
				// query string is nothing to show all points
			} else {
				// just show needs
				queryString = "&field_give_or_share_value[]=wish&field_give_or_share_value[]=wish_to_share";
			}
		} else { // don't show needs
			if (showGifts == true ) {
				// show only gifts
				queryString = "&field_give_or_share_value[]=gift&field_give_or_share_value[]=gift_to_share";
			} else {
				// neither box is checked
			}
		}
		// map is 700px by 600px, so we need a circular radius of about 450px
		if (zoom >= 9) {
			radius = 110; // 85px = 20km
		}
		if (zoom == 8) {
			radius = 215; // 105px = 50km
		}
		if (zoom == 7) {
			radius = 430; // 105px = 100km
		}
		if (zoom == 6) {
			radius = 900; // 105px = 200km
		}
		if (zoom == 5) {
			radius = 1700; // 54px = 200km
		}
		if (zoom == 4) {
			radius = 3300; // 70px = 500km
		}
		if (zoom == 3) {
			radius = 6600; // 70px = 1000km
		}
		if (zoom == 2) {
			radius = 13200; // 70px = 2000km
		}
		if (zoom <= 1) {
			radius = 60000; // 70px = 5000km
		}
	  // Read the data from the feed
	  var url = "/mapfeed.xml?distance[latitude]="+lat+"&distance[longitude]="+lng+"&distance[search_distance]="+radius+"&distance[search_units]=km"+queryString;
	  downloadUrl(url, function(doc) {
        var xmlDoc = xmlParse(doc);
		var markers = xmlDoc.documentElement.getElementsByTagName("node");
		var previousLat = '';
		var previousLng = '';
		duplicatePoints = 1;
		for (var i = 0; i < markers.length; i++) {
			// obtain the attribues of each marker
			var titles = xmlDoc.documentElement.getElementsByTagName("title");
			var title = titles[i].firstChild.nodeValue;
			var paths = xmlDoc.documentElement.getElementsByTagName("path");
			var path = paths[i].firstChild.nodeValue;
			var startdates = xmlDoc.documentElement.getElementsByTagName("startdate");
			var startdate = startdates[i].firstChild.nodeValue;
			var users = xmlDoc.documentElement.getElementsByTagName("user");
			var user = users[i].firstChild.nodeValue;
			var ratings = xmlDoc.documentElement.getElementsByTagName("rating");
			if (ratings[i].firstChild != null) {
				var rating = ratings[i].firstChild.nodeValue;
			} else {
				var rating = '';
			}
			var usernames = xmlDoc.documentElement.getElementsByTagName("username");
			var username = usernames[i].firstChild.nodeValue;
			var exchanges = xmlDoc.documentElement.getElementsByTagName("exchanges");
			if (exchanges[i].firstChild != null) {
				var exchange = exchanges[i].firstChild.nodeValue;
			} else {
				var exchange = '';
			}
			
			var images = xmlDoc.documentElement.getElementsByTagName("image");
			if (images[i].firstChild != null) {
				var image = images[i].firstChild.nodeValue;
			} else {
				var image = '';
			}
			var userpictures = xmlDoc.documentElement.getElementsByTagName("picture");
			if (userpictures[i].firstChild != null) {
				var userpicture = userpictures[i].firstChild.nodeValue;
			} else {
				var image = '';
			}
			var types = xmlDoc.documentElement.getElementsByTagName("type");
			var type = types[i].firstChild.nodeValue;
			var types2 = xmlDoc.documentElement.getElementsByTagName("type2");
			var type2 = types2[i].firstChild.nodeValue;
			var lats = xmlDoc.documentElement.getElementsByTagName("lat");
			var lngs = xmlDoc.documentElement.getElementsByTagName("lng");
			var lat = parseFloat(lats[i].firstChild.nodeValue);
          	var lng = parseFloat(lngs[i].firstChild.nodeValue);
			var newlng = '';
			
			// set for testing next point for same location
			if (checkCoords(lat,lng,previousLat,previousLng) == true) {
				// change lat slightly
				var multiplier = duplicatePoints * 0.0025;
				newlng = lng+multiplier;
				duplicatePoints++;
			}
			previousLat = lat;
			previousLng = lng;
			if (newlng != '') {
          		var point = new google.maps.LatLng(lat,newlng);
			} else {
				var point = new google.maps.LatLng(lat,lng);
			}
          	var html = '<div class="infoboxcontainer"><div style="float:right; padding-bottom:5px"><div class="'+ type +'" style="display:inline-block"> </div> <div class="'+ type2 +'" style="display:inline-block"> </div></div>';
			if (image != '' ) {
				html = html +'<a href="' + path + '"><img src="/sites/thegiftingearth.net/files/styles/thumbnail/public/' + image + '" width="100" height="82" alt="' + title + '" style="float:right; clear:right"></a>';
			} 
			// format date 
			var dateParts = startdate.split('-');
			var datedate = dateParts[2].split(' ');
			var themePath = <?php print '\'/' . drupal_get_path('theme','boron') . '\''; ?>;
			if (userpicture != 'user_thumbnail.gif') {
				bgimage = 'background-image:url(/sites/thegiftingearth.net/files/styles/user_thumbnail/public/pictures/' + userpicture + ');';
			} else {
				bgimage = 'background-image:url(' + themePath + '/images/user_thumbnail.gif);';
			}
			html = html + '<a href="' + path + '"><strong>' + title + '</strong></a><br />' +dateParts[1]+ '/' + datedate[0] + '/'+ dateParts[0]+ '<br />Posted By:<br /><div><a class="userlink" href="/user/' + user + '" style="padding-left:30px; ' + bgimage + '">' + username + '</a></div>';
			if (exchange != '' ) {
				if (rating == 100) {
					var number = 'five';
				}
				if (rating == 80) {
					var number = 'four';
				}
				if (rating == 60) {
					var number = 'three';
				}
				if (rating == 40) {
					var number = 'two';
				}
				if (rating == 20) {
					var number = 'one';
				}
				if (rating == 0) {
					var number = 'none';
				}
				if (number != 'none') {
					html = html + ' (' + exchange + ')<br /><span class="display-stars ' + number + '" style=""> </span><br />';
				} else {
					html = html + '<br /><div style="padding-top:3px"> </div>';
				}
			} 
			html = html + '<a class="btn" href="' + path + '">View Details</a><br /><span style="font-size:11px">NOTE: for privacy, the points on this map have a built-in error, and are for general planning purposes only.</span></div>';
          	var label = title; 
		  
          // create the marker
          var marker = createMarker(point,label,html,type);
        } // end for loop
      });  // end downLoadurl()
	} // end loadPoints()
	
	// zoom_changed listener
	var radius = 111; // default radius in km
	
  	
    google.maps.event.addListener(map, 'zoom_changed', function() {
		lat = map.getCenter().lat();
		lng = map.getCenter().lng();
		var nzoom = map.getZoom();
		document.getElementById("lat-center").value = lat;
		document.getElementById("lng-center").value = lng;
		document.getElementById("current-zoom").value = nzoom;
		loadPoints(map,lat,lng,nzoom);
	  }); // end zoom_changed listener
	  
	  
	  google.maps.event.addListener(map, 'dragend', function() {
	  	lat = map.getCenter().lat();
		lng = map.getCenter().lng(); 
		var nzoom = map.getZoom();
		document.getElementById("lat-center").value = lat;
		document.getElementById("lng-center").value = lng;
		document.getElementById("current-zoom").value = nzoom;
		loadPoints(map,lat,lng,nzoom);
	  }); // end dragend listener
	  
	  
	  
	  loadPoints(map,lat,lng,zoom);
	  
} // end initialize();
  
var infowindow = new google.maps.InfoWindow( { 
   size: new google.maps.Size(150,50)
});
    	
//]]>
</script>  
<?php endif; ?>
<link href='https://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="/<?php print drupal_get_path('theme','boron'); ?>/scripts/jquery.youtubepopup.min.js"></script>

</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?> <?php if ($showmap) { echo 'onload="initialize()"'; } ?>>


  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>

<script type="text/javascript"> 
//<![CDATA[
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-40570433-1', 'thegiftingearth.net');
  ga('send', 'pageview');
//]]>
</script>
</body>
</html>