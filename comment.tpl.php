<?php

/**
 * @file
 * Default theme implementation for comments.
 *
 * Available variables:
 * - $author: Comment author. Can be link or plain text.
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $created: Formatted date and time for when the comment was created.
 *   Preprocess functions can reformat it by calling format_date() with the
 *   desired parameters on the $comment->created variable.
 * - $changed: Formatted date and time for when the comment was last changed.
 *   Preprocess functions can reformat it by calling format_date() with the
 *   desired parameters on the $comment->changed variable.
 * - $new: New comment marker.
 * - $permalink: Comment permalink.
 * - $picture: Authors picture.
 * - $signature: Authors signature.
 * - $status: Comment status. Possible values are:
 *   comment-unpublished, comment-published or comment-preview.
 * - $title: Linked title.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the following:
 *   - comment: The current template type, i.e., "theming hook".
 *   - comment-by-anonymous: Comment by an unregistered user.
 *   - comment-by-node-author: Comment by the author of the parent node.
 *   - comment-preview: When previewing a new or edited comment.
 *   The following applies only to viewers who are registered users:
 *   - comment-unpublished: An unpublished comment visible only to administrators.
 *   - comment-by-viewer: Comment by the user currently viewing the page.
 *   - comment-new: New comment since last the visit.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * These two variables are provided for context:
 * - $comment: Full comment object.
 * - $node: Node object the comments are attached to.
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_comment()
 * @see template_process()
 * @see theme_comment()
 */
 
 // check if the comment is owner by the giftor or gfitee
$field_giftingearth_node_subtype = $node->field_giftingearth_node_subtype['und'][0]['value'];
if ($field_giftingearth_node_subtype != 'ge_listing_node') {
	$listing_nid = ge_get_parent_exchange_node_id($node->nid);
	$othernode = node_load($listing_nid);
	$field_give_or_share = $othernode->field_give_or_share['und'][0]['value'];
	if (($field_give_or_share == "gift") or ($field_give_or_share == "gift_to_share")) {
		$originalnodeauthor = 'giftor';
	} else {
		$originalnodeauthor = 'giftee';
	}
	if ($othernode->name == $comment->name) {
		$commentclass = $originalnodeauthor;
	} else { // just display the opposite
		if ($originalnodeauthor == 'giftor') {
			$commentclass = 'giftee';
		} else {
			$commentclass = 'giftor';
		}
		
	}
}
?>
<article class="<?php print $classes; ?> <?php echo $commentclass; ?> clearfix"<?php print $attributes; ?>>

  <header class="submittedby">
    <?php //print $picture ?>

    <?php if ($new): ?>
      <span class="new"><?php print $new ?></span>
    <?php endif; ?>

    <!--<?php print render($title_prefix); ?>
    <h3<?php print $title_attributes; ?>><?php print $title ?></h3>
    <?php print render($title_suffix); ?>-->

    <p class="submitted">
      <?php //load user data to get real name
      $cuser = user_load($comment->uid);
      $name = l($cuser->field_user_firstname['und'][0]['safe_value'].' '.$cuser->field_user_lastname['und'][0]['safe_value'], 'user/'.$comment->uid, array('attributes' => array('class' => 'username')));
      print t('Submitted by !username',
          array('!username' => $name)); ?><br />
      <?php
        print t('!datetime.',
          array('!datetime' => $created));
      ?>
    </p>
  </header>

  <div class="content"<?php print $content_attributes; ?>>
    <?php
      // We hide the links now so that we can render them later.
      hide($content['links']);
      print render($content);
    ?>
    <?php if ($signature): ?>
      <div class="user-signature clearfix">
        <?php print $signature ?>
      </div> <!-- /.user-signature -->
    <?php endif; ?>
  </div> <!-- /.content -->

  <?php if (!empty($content['links'])): ?>
    <footer>
      <?php print render($content['links']) ?>
    </footer>
  <?php endif; ?>

</article> <!-- /.comment -->
