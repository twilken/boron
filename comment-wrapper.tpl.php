<?php

/**
 * @file
 * Default theme implementation to provide an HTML container for comments.
 *
 * Available variables:
 * - $content: The array of content-related elements for the node. Use
 *   render($content) to print them all, or
 *   print a subset such as render($content['comment_form']).
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default value has the following:
 *   - comment-wrapper: The current template type, i.e., "theming hook".
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * The following variables are provided for contextual information.
 * - $node: Node object the comments are attached to.
 * The constants below the variables show the possible values and should be
 * used for comparison.
 * - $display_mode
 *   - COMMENT_MODE_FLAT
 *   - COMMENT_MODE_THREADED
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess_comment_wrapper()
 * @see theme_comment_wrapper()
 */

// test if this is a listing or response node
$render_tge_comment = render_tge_comment($user,$node);
$display = FALSE;
$field_giftingearth_node_subtype = $content['#node']->field_giftingearth_node_subtype['und'][0]['value'];
if ($field_giftingearth_node_subtype == 'ge_listing_node') {
	$ge_listing_node = TRUE;
} else {
	$ge_listing_node = FALSE;
}

// get id of original listing node
if (isset($content['#node']->field_parent_exchange_nid['und'][0]['value'])) {
	$field_parent_exchange_nid = $content['#node']->field_parent_exchange_nid['und'][0]['value'];
} else {
	$field_parent_exchange_nid = NULL;
}
// check if parent is listing node
$parent_is_listing_node = ge_is_parent_listing_node($field_parent_exchange_nid);

// get id of original listing node owner
if (isset($content['#node']->field_parent_exchange_uid['und'][0]['value'])) {
	$field_parent_exchange_uid = $content['#node']->field_parent_exchange_uid['und'][0]['value'];
} else {
	$field_parent_exchange_uid = NULL;
}

// check ownership
global $user;
//echo '$content[\'#node\']->uid: '.$content['#node']->uid.'<br>';
//echo '$user-uid: '.$user->uid;
if ($content['#node']->uid == $user->uid) {
	$owner = TRUE;
} else {
	$owner = FALSE;
}

$field_give_or_share = $content['#node']->field_give_or_share['und'][0]['value'];
$gift_response = FALSE;
$need_response = FALSE;
switch($field_give_or_share) {
	case "gift":
		$gift_response = TRUE;
		break;
	case "wish":
		$need_response = TRUE;
		break;
	case "gift_to_share":
		$gift_response = TRUE;
		break;
	case "response_for_gift":
		$gift_response = TRUE;
		break;
	case "response_for_gift_share":
		$gift_response = TRUE;
		break;
	case "response_for_wish":
		$need_response = TRUE;
		break;
	case "response_for_wish_share":
		$need_response = TRUE;
		break;
}
// get username of other person depending on who is viewing the node
if ($node->uid == $user->uid) {
	// get username of other partner
	$getuser = user_load($field_parent_exchange_uid);
	$partner_username = $getuser->field_user_firstname['und'][0]['safe_value'].' '.$getuser->field_user_lastname['und'][0]['safe_value'];
} else {
	// username of this nodes owner will work
	$getuser = user_load($node->uid);
	$partner_username = $getuser->field_user_firstname['und'][0]['safe_value'].' '.$getuser->field_user_lastname['und'][0]['safe_value'];
}
$otheruser = user_load($field_parent_exchange_uid);
if ($ge_listing_node == FALSE) {
	// hide comment form from users not related to exchange
	if (($user->uid == $node->uid) or ($user->uid == $otheruser->uid)) {
		$display = TRUE;
	}
} else {
	$display = TRUE;
}
$display = TRUE; // 
if ($display == TRUE):
?>
<section id="comments" class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php //if ($content['comments']): ?>
    <?php print render($title_prefix); ?>
    <h2 class="title"><?php print t('Messages'); ?></h2>
    
    <?php print render($title_suffix); ?>
  <?php endif; ?>
  <?php print render($content['comments']); ?>

  <?php //if (($content['comment_form']) and ($render_tge_comment) and ($node->comment_count > 0)): ?>
  <?php //if (($content['comment_form']) ?>
  <?php //echo $node->comment_count; ?>
    <section id="ge-comment-form-wrapper" style="clear:both">
      
        <?php if ($gift_response): ?>
        	<?php if ($owner): ?>
           		<?php if ($field_give_or_share == 'gift'): ?>
                    <h2 class="title"><?php print t('Send public message to GIFTee '); ?></h2>
    			<?php else: ?>
                	<h2 class="title"><?php print t('Send public message to GIFTor'); ?></h2>
    			<?php endif; ?>
            
            <?php else: ?>
            	<?php if (($field_give_or_share == 'response_for_gift_share') or ($field_give_or_share == 'gift')): ?>
                    <h2 class="title"><?php print t('Send public message to GIFTor'); ?></h2>
    			<?php else: ?>
                	<h2 class="title"><?php print t('Send public message to GIFTee'); ?></h2>
    			<?php endif; ?>
            
            <?php endif; ?>
		<?php else: // is a need response ?>
        	<?php if ($owner): ?>
            	<?php if (($field_give_or_share == 'response_for_wish') or ($field_give_or_share == 'wish')): 
					if ($parent_is_listing_node) : ?>
                    <h2 class="title"><?php print t('Send public message to GIFTor'); ?></h2>
                    <?php else: ?>
                    <h2 class="title"><?php print t('Send public message to GIFTee'); ?></h2>
                    <?php endif; ?>
    			<?php else: ?>
                    <h2 class="title"><?php print t('Send public message to GIFTor'); ?></h2>
                <?php endif; ?>
            <?php else: ?>
				<?php if (($field_give_or_share == 'wish') or ($field_give_or_share == 'response_for_wish')): ?>
                    <h2 class="title"><?php print t('Send public message to GIFTor'); ?></h2> 
                <?php else: ?>
                	<h2 class="title"><?php print t('Send public message to GIFTee'); ?></h2>
				<?php endif; ?>
            <?php endif; ?>
        <?php endif; ?>
     <div class="form-item">
     	<div class="description">
    		<?php print t('The form below will send a message to '); ?><em><?php echo $partner_username; ?></em> <?php print t('without changing the status of this gifting event.'); ?>
       		<?php print t('This message and the others above will be publicly visible after the gifting process is concluded.'); ?> 
       		<a href="/messages/new/<?php echo $content['#node']->uid; ?>?destination=node/<?php echo $content['#node']->nid; ?>&subject=<?php echo urlencode($content['#node']->title); ?>"><?php print t('Click here to send a private message'); ?></a>
   		</div>
   	</div>
      	<?php // old contact form
      	//$content['comment_form']['actions']['submit']['#value'] = t('Send Message');
		//print render($content['comment_form']); 

		// new contact form
		$form_variables['ge_comment_form_nid'] = $node->nid;
		print drupal_render(drupal_get_form('ge_comment_form', $form_variables)); ?>
    </section> <!-- /#comment-form -->
  <?php //endif; ?>
</section> <!-- /#comments -->

<?php //endif; ?>
